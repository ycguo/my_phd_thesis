\section{Diboson Resonances}
\label{sec:theory_diboson}

In the ATLAS experiments, we search for new particles by reconstructing their decay products. 
Many BSM theories predicted new phenomena that can be reconstructed as diboson resonances in the 
experiments, such as the graviton ($G$) in the Randall Sundrum models, the $W'$ in the heavy vector triplet 
model, and a high mass scalar under narrow width assumption. 
Particularly, we are interested in $ZZ$ and $WZ$ resonances. 
\\

\subsection{Randall Sundrum Models}
\label{subsec:theory_rs}

In 1999, Lisa Randall and Raman Sundrum proposed a model (namely RS1) to solve the Higgs hierarchy problem 
by introducing a warped extra-dimension \cite{PhysRevLett.83.3370}, since they are not satisfied with extra 
dimension models which aimed to solve to hierarchy problem but introduced new hierarchy. Later in same year,
they published a second model introducing a non-compact extra dimension, in which all SM particle can propagate,
namely RS2 \cite{PhysRevLett.83.4690}.  
\\

In the four-dimensional spacetime, the fundamental scale, also know as the Planck scale, is about $10^{19}$ GeV. 
The standard model implies that the Higgs mass received a correction at the fundamental scale and get a mass 
at weak scale ( $\sim 100$ GeV). Experiments have proved the Higgs mass is $125$ GeV. Such a precise
correction causes naturalness arguments. The problem could be solved if the the fundamental scale is not that
large. Assuming the spacetime is fundamentally higher dimensional with $4+n$ spacetime dimensions, the 
4-dimensional Planck scale $M_{pl}$ is determined by the fundamental $(4+n)$-dimensional Planck scale $M$ and 
the geometry of the extra dimensions. If the higher dimensional spacetime is simply a product of a 4-dimensional 
spacetime with a n-dimensional compact space, then
\begin{equation}
   M_{pl}^2 = M^{n+2} V_n,
\end{equation}
where $V_n$ is the volume of the compact space \cite{PhysRevLett.83.3370}. A straightforward solution to eliminate
the large hierarchy is taking a very large compact space, as proposed in the large extra dimension model 
\cite{Antoniadis:1998ig}. But this model introduces a new hierarchy, between the compactification scale 
$\mu_c \sim 1/V_n^{1/n}$ and the weak scale. 
\\

The RS1 model \cite{PhysRevLett.83.3370}  provides a new solution, without requiring the fundamental scale to be 
as small as the weak scale. With a warped extra dimension, the metric becomes 
\begin{equation}
   ds^2 = e^{(-2k r_c \phi)} \eta_{\mu \nu} dx^{\mu} dx^{\nu} + r_c^2 d{\phi}^2,
\end{equation}
where $k$ is a scale of order the Planck scale, $x_\mu$ are coordinates for the familiar four dimenions, while 
$0 \leq \phi \leq \pi$ is the coordinate for an extra dimension, which is a finite interval whose size is set by $r_c$.
By such a metric the Higgs vacuum expectation value is exponentially suppressed,
\begin{equation}
  v = e^{(-k r_c \pi)} v_0.
\end{equation}
and the mass hierarchy is thus naturally solved,
\begin{equation}
  m = e^{(-k r_c \pi)} m_0.
\end{equation}

The Planck scale, however, is weakly dependent on $r_c$
\begin{equation}
\label{eq:rs_mpl}
  M_{pl}^2 = \frac{M^3}{k} (1-e^{(-2k r_c \pi)})
\end{equation}

Both the the large-extra dimension model and the RS1 model introduce compact dimension(s) in which only the graviton
can propagate, while the SM particles are confined in the $(3+1)$ dimensional spacetime. In the RS2 model, with a
non-compact warped extra dimension, all the SM particles can propagate in the $(4+1)$ dimension spacetime, as well
as the graviton. The most dramatic consequence could be that we can live in $4+n$ non-compact dimensions, 
in perfect compatibility with experimental gravity. We are interested in the RS2 model in this thesis. In this model, the
gravitons are produced mainly via the gluon-gluon fusion (ggF), as illustrated in Figure~\ref{fig:fm_rsg}. 
The coupling strength depends on $k / \bar{M}_{pl}$ where the $\bar{M}_{pl} = 2.4 \time 10^{18}$ GeV is the reduced 
Planck scale. The production cross section and decay width of the graviton scale as the square of  $k / \bar{M}_{pl}$.
\\
\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.35\textwidth]{figures/introduction/fm_rsg_ggF.pdf}
  \caption {Feynman diagram showing the Kaluza–Klein (KK) excitations of gravitons via ggF production.}
  \label{fig:fm_rsg}
\end{figure}


\subsection{Heavy Vector Triplet Model}
Extra gauge bosons are common occurrence in BSM theories. In BSM models such as the Grand Unified Theories, 
including string constructions, or Little Higgs models, new vector bosons appear as the gauge bosons of the extra 
broken symmetries. Vector resonances are a typical feature of theories with a strongly coupled sector, such as 
composite Higgs models. However, these theories are not developed enough to provide sharp predictions of the 
experimental observables. Searching new vectors over a large set of parameters becomes computational expensive.
The heavy vector triplet  (HVT) model \cite{deBlas:2012qp} \cite{Pappadopulo:2014qza} provides a phenomenological
framework that encompasses different scenarios involving new vector bosons and their couplings to SM fermions and 
bosons. The model introduced a triplet $\tpW$ that can be described as a set of three gauge bosons, two charged
$W'^{\pm}$ and one neutral, $Z'$. The interaction Lagrangian between $\tpW$ and SM particles is,
\begin{equation}
   \lagr_{\tpW}^{int} = - g_q \tpW_{\mu}^a \bar{q}_k - g_\ell \tpW_{\mu}^a \bar{\ell} \gamma^{\mu} \frac{\sigma_a}{2} \ell_k
                                  - g_H ( \tpW_{\mu}^a  H^{\dagger}  \frac{\sigma_a}{2} D^{\mu} H + h.c.),
\end{equation}
where $q_k$ and $l_k$ represent the left-handed quark and lepton doublet for fermion generation $k (k = 1, 2, 3)$;
$H$ represents the Higgs doublet; $\sigma_a (a = 1, 2, 3)$ are the Pauli matrices; $g_q$, $g_\ell$ and $g_H$ 
correspond to the coupling strengths between $\tpW$ and the quark,  lepton and Higgs fields, respectively 
\cite{Aaboud:2018bun}.
\\

The two main production mechanisms of the new vector bosons in the HVT model are Drell-Yan (DY) and vector boson 
fusion (VBF). The diagrams are shown in Figure~\ref{fig:fm_hvt}
Two explicit scenarios are considered in this thesis with DY production, as well as a scenario with VBF
production. The two DY models, namely model A and model B, are representatives for weakly and strongly coupled 
theories. The weakly coupled model A describes extended gauge symmetry \cite{PhysRevD.22.727}. The couplings 
are $g_H = -0.56$ and $g_f = -0.55$ with the universal fermion coupling $g_f = g_q = g_\ell$. The strong coupled 
model B considers the minimal Composite Higgs model \cite{Contino:2011np} with $g_H = -2.9$ and $g_f = -0.14$. 
The last scenario, referred to as model C, is designed to focus on the rare process of VBF.  In this case, the couplings 
are set to $g_H = 1$ and $g_f = 0$ \cite{Aaboud:2018bun}. Thus the phase space of model C is very different from 
model A and B. The different productions mechanisms provide sensitivity to different regions of the parameter space.
\\

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.3\textwidth]{figures/introduction/fm_DY.pdf}} \quad
  \subfloat[]{\includegraphics[width=0.31\textwidth]{figures/introduction/fm_VBF.pdf}}
  \caption {Example diagrams for (a) DY and (b) VBF productions for HVT, where $X$ represents the charged vectors
  $W'^{\pm}$.} 
  \label{fig:fm_hvt}
\end{figure}
