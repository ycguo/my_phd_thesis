\section{Jets}
\label{sec:obj_jets}

Known as the color confinement, a single quark or gluon, which carries color charge, cannot exist in free. 
Quarks and gluons produced or scattered in the collision will generate other color objects to from colorless
hadrons in the detector. The process is called hadronization. The shower of particles originating from the 
hadronization and fragmentation of a strongly-interacting particle produced in a high-energy particle collision
is reconstructed experimentally as a jet\cite{ATLAS-CONF-2017-062}. 
Hadrons deposit energy in the hadron calorimeter and the charged ones among them leave tracks in the inner 
detector (ID). Thus jets are built from the ID tracks and/or the topo-clusters of the calorimeter energy cells,
with calibrations applied on the topo-clusters to correct their energy and overall correction.
\\


\subsection{Jet Reconstruction}
Jets are reconstructed using reclustering algorithms,  which typically fall under two big categories: cone algorithms
 and sequential-recombination algorithms \cite{Marzani:2019hun}. In this section, we will focus on a widely used jet algorithm, 
 the anti-$k_t$ algorithm\cite{Cacciari:2008gp}.
%Jets reconstruction from ID tracks are same to the charged track reconstruction discussed in Section~\ref{sec:obj_tracks}. 

\paragraph{Anti-$k_t$ algorithm}
The anti-$k_t$ algorithm is the most widely used jet recombination algorithm in the ATLAS experiment. 
Deriving from $k_t$ \cite{Ellis:1993tq} and Cambridge/Aachen \cite{Dokshitzer:1997in} \cite{Wobisch:1998wt} inclusive 
jet finding algorithms, this algorithm provides a natural, fast, infrared and collinear safe replacement \cite{Cacciari:2008gp}. 
\\

The algorithm starts by generalizing the the $k_t$ \cite{Ellis:1993tq} and Cambridge/Aachen algorithms. 
It collects an initial list of protojets, and recursively groups pairs of protojets together to form new protojets.
The grouping criteria introduce two distance variables, $d_{i, j}$ as distances between entities $i$ and $j$ and 
$d_{i, B}$ between entity i and  the beam $B$, where an entity is a protojet. 

\begin{subequations}
\begin{equation}
\label{eq:obj_jet_antikt_d}
d_{i,j} = min(k_{ti}^{2p}, k_{ti}^{2p})\frac{\Delta_{ij}^2}{R^2}
\end{equation}

\begin{equation}
d_{i,B} = k_t^p
\end{equation}
\end{subequations}

where $\Delta_{i,j}^2 = (y_i − y_j)^2 + (\phi_i − \phi_j)^2$ and $k_{ti}$, $y_i$ and $\phi_i$ are respectively the transverse momentum, 
rapidity and azimuth of particle $i$. $R$ is a given radius parameter when processing the algorithm. 
A term $p$ is introduced to govern the relative power of the 
energy versus geometrical ($\Delta_{i,j}$) scales. By choosing different $p$ one can get different jet recombination algorithms.
If one choice $p = 1$, it becomes the $k_t$ algorithm. For $p > 0$, the behavior of soft terms in the algorithm is similar to $k_t$ algorithm. 
With $ p = 0$, it is the inclusive Cambridge/Aachen algorithm. Since the algorithm behaviors on soft radiation terms are similar when
$p < 0$, without loss of generality,  the anti-$k_t$ algorithm concentrate on $p = -1$. 

The grouping is processed recursively as follows \cite{Ellis:1993tq},
\begin{enumerate}
 \item For each protojet, find the smallest of all the $d_{i,B}$ and $d_{i,j}$ and label it $d_{\text{min}}$.
 \item If $d_{\text{min}}$ is  $d_{i,j}$, merge $i$ and $j$ to a new protojet $m$ with
	\begin{subequations}
        \begin{equation}
	  k_{tm} = k_{ti} + k_{tj}
	\end{equation}

	\begin{equation}
	  \eta_m = (\eta_i * k_{ti} + \eta_j * k_{tj}) / k_{tm}
	\end{equation}
	
	\begin{equation}
	  \phi_m = (\phi_i * k_{ti} + \phi_j * k_{tj}) / k_{tm}
	\end{equation}
	\end{subequations}
\item If $d_{\text{min}}$ is  $d_{i,B}$, the protojet protojet $i$ is “not mergable”. 
  Remove it from the list of protojets and add it to the list of jets.
\item Repeat step 1-3 until no protojet left.
\end{enumerate}


The behavior of the anti-$k_t$ algorithm could be understood by examining typical examples.
Given an event with a few well-separated hard particles with transverse momenta $k_{t1}$, $k_{t2}$, ..., 
and many soft particles, according to Equation~\ref{eq:obj_jet_antikt_d}, the distance between the hard particle 1 
and a soft particle $i$ is $d_{1,i} = min(k_{t1}^{2p}, k_{ti}^{2p}) \frac{\Delta_{1i}^2}{R^2}$, which is determined
by the transfers momentum of the hard particle and the separation $\Delta_{1i}^2$ between two particles.
The distance $d_{ij}$ of two soft particles with similar separation would be much bigger. Thus the soft terms 
tend to cluster with hard cores. In other words, the hard particle dominates the clustering in the anti-$k_t$ algorithm.
Considering the distribution of hard particles, for particle 1,
\begin{itemize}
  \item If the hard particle has no neighbors within a distance of $2R$, it will absorb soft particles within a circle of
  radius $R$.
  \item If a hard particle 2 exists with $R < \Delta_{1,2} < 2R$, the soft terms in overlapping region will be divided to 
  two parts according to the momentum of the two hard particles. When $k_{t1}$ and $k_{t2}$ are in a same magnitude,
  the boundary b is defined by $\Delta_{1b} / k_{t1} = \Delta_{2b} / k_{t2}$
  \item If a hard particle 2 exists with $\Delta_{1,2} < R$, the two hard particles will merged to form a single jet.
\end{itemize}


This behavior, along with a comparison with different jet reconstruction algorithms are shown in Figure~\ref{fig:obj_jet_algorithms}.
The resilience of jet boundaries with respect to soft radiations, confirmed that the anti-$k_t$ algorithm is infrared and collinear (IRC)
safe.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/objects/jet_algorithms_behaviour.pdf}
  \caption {A sample parton-level event together with many random soft “ghosts”, clustered with four different jets algorithms, 
  illustrating the “active” catchment areas of the resulting hard jets
  \cite{Cacciari:2008gp}
  }. 
  \label{fig:obj_jet_algorithms}
\end{figure}


\paragraph{Jet Collections}

As mentioned, inputs for jet reconstruction are tracks and topo-clusters of calorimeters. Tracks are given by the precision 
tracking system as introduced in Section~\ref{sec:obj_tracks}, used to build track jets. Topo-clusters are topological groups of noise-suppressed 
calorimeter cells. Those clusters can be at the EM-scale or with local cluster weighting (LCW). 
\\

A jet collection is reconstructed from certain jet inputs by a jet reconstruction algorithm with specific settings,  to satisfy the requirements 
of physics analyses. Here we introduce four widely used jet definitions.
\begin{itemize}
  \item Track jets reconstructed from precision tracks, with very small radius. Usually trackjets are not treated as signal jets due to 
  their tiny radius. But they are important objects used to define other objets like large radius jets and determine a phase space. 
  The trackjet collection used in this paper is AntiKt2LCTopoJet. % which means these jets are reconstructed by anti-$k_t$ 
  % algorithm with $R = 0.2$, 
  % track jets
  \item Small-radius (small-R) jets with $R = 0.4$ is one of the most widely used jet definition. These jets are built from topological clusters,
  formed from calorimeter cell deposits, and calibrated to the electromagnetic (EM) scale (AntiKt4EMTopoJet). In our study, jets not 
  representative of a boosted boson are reconstructed into small-R jets.
  \item Large-radius (large-R) jets with $R = 1.0$ is another widely used jet definition. 
  These jets are built from topological clusters, formed from calorimeter cell deposits, and calibrated using the LCW scheme.  
  After reconstructed by the anti-$k_t$ algorithm, trimming procedure is applied~\cite{Krohn2010}. 
  The initial jet constituents are reclustering into subjets of radius $R_{sub}$ by anti-$k_t$ algorithm. 
  Then any subjet that has a transverse momentum that is less than $f_{cut}$ times the transverse momentum of the
  parent jet are removed. The recommended values for the two parameters are: $R_{sub} = 0.2$ and $f_{cut} = 5\%$. 
  The remained constituents are merged into the large-R jet. 
  The energy and mass of the jet are then calibrated to the truth level particle jet energy scale (JES) 
  and jet mass scale (JMS) as derived by the JetEtMiss group.  
  Furthermore, any substructure variable used here is calculated using only those clusters that pass trimming. 
  In many analyses, large-R jets are used to reconstruct decay product of boosted particles such as $Z$ and $W$ bosons, 
  since jets from a boosted quark pair tend merged into a single fat jet. 
  \item TCC Large-R Jets. 
\end{itemize}


%\subsection{Jet Calibration}
% Calibration

\subsection{Jet Substructure}
\label{sec:obj_jet_sub}
As discussed above, jets are collimated flow of hadrons, so the substructure of jets (i.e. energy distribution inside jets) is interesting, 
especially for large-radius jets. Figuring out the energy distributions helps analyzers to distinguish quark/gluon jets and identify particles 
such as $W$, $Z$, $H$, and top-quarks. 
Many estimators building from energy distribution were proposed, N-subjettiness \cite{Thaler:2010tr} \cite{Thaler:2011gf}  simply based on
the transverse momentum and the distance in the rapidity-azimuth plane  of constituent particles, 
$C$-parameters \cite{Larkoski:2013eya}, $D$-parameters \cite{Larkoski:2014gra} based on energy correlation functions. 
The primary motivation of using jet substructure variables in our studies is to distinguish a two-prong jet, such as those decayed from $W$ or $Z$.
In this case, $D_2^{(\beta)}$  is the most commonly used jet substructure variable and has the best performance in our study.
\\

\paragraph{Energy Correlation Function}
The N-point energy correlation function (ECF) for hadron collider experiments is \cite{Larkoski:2013eya},

\begin{equation}
\text{ECF}(N, \beta) = \sum_{i_1 < i_2 < ... < i_N \in J } (\prod_{a=1}^{N} p_{T_{i_a}} )(\prod_{n=1}^{N-1} \prod_{c=b+1}^{N} R_{i_{b}i_{c}})^{\beta}
\end{equation}

where $\Delta_{i,j}^2 = (y_i − y_j)^2 + (\phi_i − \phi_j)^2$ is the Euclidean distance between $i$ and $j$ in the rapidity-azimuth angle plane.
The sum runs over all particles within the system J (either a jet or the whole event). This function is IRC safe for $\beta > 0$. For N up to 3,
we have,
\begin{subequations}
\label{eq:obj_jet_ecf1}
\begin{equation}
\text{ECF}(0, \beta) = 1
\end{equation}
\begin{equation}
\text{ECF}(1, \beta) = \sum_{i \in J } ( p_{T_i} p_{T_j})
\end{equation}
\begin{equation}
\text{ECF}(2, \beta) = \sum_{i < j \in J } ( p_{T_i} p_{T_j})(R_{ij})^{\beta}
\end{equation}
\begin{equation}
\text{ECF}(3, \beta) = \sum_{i < j < k \in J } ( p_{T_i} p_{T_j} p_{T_k})(R_{ij}R_{jk}R_{ki})^{\beta}
\end{equation}
\end{subequations}

A key observation of ECF is that $\text{ECF}(N,β) = 0$ if the jet has fewer than N constituents. Thus, the (N+1)-point energy correlation function
should be significantly smaller than the N-point energy correlation function, for a system with N partons. From this observation 
we can build a dimensionless observable,
\begin{equation}
C_N^{(\beta)} = \frac{\text{ECF}(N+1, \beta)\text{ECF}(N-1, \beta) }{\text{ECF}(N, \beta)^2}.
\end{equation}
This is the so-called $C$-parameters as mentioned above. In order to select two-prong jets, one can use $C_2^{(\beta)}$. For two-prong jets,
$C_2^{(\beta)}$ is close to zero and is much bigger in other cases. 

\paragraph{$D_2^{\beta}$}
A study \cite{Larkoski:2014gra} of dimensionless  N-point energy correlation function proposed a more powerful observable $D_2^{\beta}$ in the discrimination of $Z$
versus QCD. The dimensionless  ECF is,
\begin{equation}
\label{eq:obj_jet_ecf2}
e_n^{\beta} = \frac{\text{ECF}(N, \beta)}{\text{ECF}(1, \beta)^N}
\end{equation}

Combining Equation~\ref{eq:obj_jet_ecf2} with Equation~\ref{eq:obj_jet_ecf1}, the two- and three-point ECFs can be written as,

\begin{subequations}
\begin{equation}
e_2^{\beta} = \frac{\text{ECF}(2, \beta)}{\text{ECF}(1, \beta)^2} = \frac{1}{p_{TJ}^2} \sum_{i < j \in J } ( p_{T_i} p_{T_j})(R_{ij})^{\beta}
\end{equation}
\begin{equation}
e_3^{\beta} = \frac{\text{ECF}(3, \beta)}{\text{ECF}(1, \beta)^3} =  \frac{1}{p_{TJ}^3} \sum_{i < j < k \in J } ( p_{T_i} p_{T_j} p_{T_k})(R_{ij}R_{jk}R_{ki})^{\beta}
\end{equation}
\end{subequations}
where $p_{TJ}$ is the total transverse momentum of the jet with respect to the beam. $D_2^{\beta}$  is defined as,
\begin{equation}
D_2^{\beta} = \frac{e_3^{\beta}}{(e_2^{\beta})^3},
\end{equation}
Both theoretical analysis and Monte-Calor simulation in the study \cite{Larkoski:2014gra}  shows that $D_2^{\beta}$  has better separation power 
between 1-prong and 2-prong jets than $C_2^{\beta}$. Figure~\ref{fig:obj_jet_sub_c2_vs_d2} shows that as $\beta$ decrease from 2,  
the discrimination power of $C_2^{\beta}$ decrease substantially, however $D_2^{\beta}$ is more robust.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/objects/jet_sub_C2_vs_D2.pdf}
  \caption {Signal and background distributions for the ratio observables $C_2^{\beta}$ (left) and $D_2^{\beta}$ (right) for $\beta = 0.5,1,2$ \cite{Larkoski:2014gra}.
  }
  \label{fig:obj_jet_sub_c2_vs_d2}
\end{figure}


