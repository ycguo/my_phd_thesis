\section{Electrons}
\label{sec:obj_electrons}

\subsection{Electron Reconstruction}

Electrons in the precision region of the ATLAS detector, where $|\eta| < 2.47$,
are reconstructed. The reconstruction of electrons candidates has three steps,
based on three fundamental components signalizing the signature of electrons: 
localized clusters of energy deposit  found within the electromagnetic (EM) calorimeter, 
charged-particle tracks identified in the inner detector, and close matching in 
$\eta \times \phi$ space of the tracks to the cluster to from the final electron 
candidates\cite{Aaboud:2019ynx}. An illustration of the  path of an electron through 
the ATLAS detector is shown in Figure~\ref{fig:obj_electron_path}.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/objects/electron_path.pdf}
  \caption {A schematic illustration of the path of an electron through the detector. 
  \cite{Aaboud:2019ynx}.
  }. 
  \label{fig:obj_electron_path}
\end{figure}

Seed-cluster reconstruction searches for EM energy cluster candidates of electrons. 
The EM calorimeter is segmented in $200 \times 256$ towers in the $\eta \times \phi$ 
space, where each tower has a size of $\eta \times \phi = 0.025 \times 0.025$.
EM cluster candidates are seeded from energy deposit with summed transfers energy 
 ($E_T$) grater than $2.5$ GeV, using a sliding window algorithm with a window size of 
$3 \times 5$ towers in $\eta \times \phi$. From simulation, the reconstruction efficiency 
range from $65$\% at $E_T = 4.5$ GeV, to $96$\% at $E_T = 7$ GeV, to more than
$99$\% above $E_T = 15$ GeV. 
\\

Track reconstruction of electron candidates is based on the track reconstruction 
described in Section~\ref{sec:obj_tracks}. The tracks with at least four his in the 
Pixel and SCT detectors and are loosely matched to EM clusters are considered
in the electron track reconstruction. The loose matching criteria is,
\begin{itemize}
  \item $|\eta_{cluster} - \eta_{track}| < 0.05$,
  \item $−0.20 < q \times [\Delta(\phi_{cluster}, \phi_{track})] < 0.05$ or 
   $ -0.1 < q \times \Delta\phi_{res} < 0.05$, where q is the sign of electric charge of 
   the particle and $\Delta\phi_{res}$ is the azimuthal separation between the cluster 
   position and the track \cite{Aaboud:2019ynx}.
\end{itemize}

A Gaussian-sum filter (GSF) is applied to the track clusters of raw measurements. 
Compared to the ATLAS Global track fitter used in track reconstruction, GFS 
account for energy of charged particles in material better.
\\

Electron candidate reconstruction matches GSF-tracks to the calorimeter candidates with 
a tighter requirement ($−0.10 < q \times [\Delta(\phi_{cluster}, \phi_{track})] < 0.05$, others 
requirements remain the same) 
and finally reconstructs electrons. 
A calorimeter candidate with an associated track which has at least four hist in Pixel or SCT 
layers and not associated with a vertex from a photon is considered as an electron candidate. 
The final reconstruction of clusters simply extends the window size of the seed clusters,  
to $3 \time 7$ and $5 \time 5$ towers in the barrel region ($|\eta| < 1.37$) and 
endcap region ($1.52 < |\eta| < 2.47$), respectively.
Above pT = 15 GeV, the efficiency to reconstruct an electron having a track of good quality 
(at least one pixel hit and at least seven silicon hits) varies from approximately 
$97$\% to $99$\% \cite{Aaboud:2019ynx}.
\\


\subsection{Electron Identification}

The identification of electrons are based on a likelihood (LH) method, to which the inputs
are the quantities from the tracking system, the calorimeter and the combined 
information. Likelihoods for signals ($L_S$) and for backgrounds ($L_B$ )are defined as
\begin{equation}
L_{S(B)}(\symbf{x}) = \prod_{i=1}^{n}{P_{S(B),i}(x_i)},
\end{equation}

where $\symbf{x}$ if the vector of input quantities and $P_{S(B),i}$ is the value of the PDF 
for input quantity $i$ at value $x_i$. 
A discriminant for electron LH identification is formed by 
\begin{equation}
d_L = \frac{L_S}{L_S + L_B}
\end{equation}

However, $d_L$ has a sharp peak around zero which leads to difficulties for selecting 
working points. The distribution of the discriminant is therefore transformed by an inverse 
sigmoid function,
\begin{equation}
d\prime_L = - \tau^{-1} \ln{(d_L^{-1} - 1)},
\end{equation}
where $\tau$ is fixed to 15. 
\\

To cover different requirements of electron signal efficiencies and background rejection
rates in physics analyses, four working points are carried out using the LH discriminant,
namely VeryLoose, Loose, Medium, and Tight. As an example, the corresponding 
efficiencies for identifying a electron with $E_T = 40$ GeV are $93$\%, $88$\%, $80$\%,
for the Loose, Medium, and Tight operating points, respectively \cite{Aaboud:2019ynx}.
\\
