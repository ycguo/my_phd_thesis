% !TeX root = ../main.tex

\chapter{Event Reconstruction and MC Simulation}
\label{ch:reconstruction}









Monte Carlo simulations of physics events, including detailed simulation of the detector response, are essential for high-energy physics experiments. They are indispensable for both the design and the development of the reconstruction and identification algorithm, as well as the data analysis and physics results interpretations. In ATLAS, at the LHC design luminosity of 1034cm−2s−1, approximately 1000 particles will emerge from the collision point every 25 ns within |η| < 2.5, putting stringent challenges to the event reconstruction software. The simulation and reconstruction programs are integrated into the ATLAS software framework, and will be briefly discussed in this Chapter, following closely the References \cite{Aad:2009wy}\cite{Aad:2010ah}.
\\

\section{Object Reconstruction}

The raw collision data recorded by the ATLAS detector passing the online trigger selection, or the raw simulated events, are reconstructed into physics objects. The ATLAS reconstruction is also based on the Athena framework. This section describes the reconstruction algorithms of the physics objects, such as tracks, vertices, electrons, muons, jets and $\MET$ .

\subsection{Electron}
  
Many of the interesting physics processes to be measured at the LHC have a signature with one or more isolated electrons. These electrons can be subject to large backgrounds, such as misidentified hadrons, electrons from photon conversions, and non-isolated electrons originating from heavy-flavour decays. It is therefore important to efficiently reconstruct and identify electrons over the full acceptance of the detector, while at the same time to have a significant background rejection. In ATLAS, this is accomplished using a combination of powerful detector technologies: silicon detectors and TRT to identify the track of the electron and a longitudinally layered electromagnetic calorimeter system with fine lateral segmentation to measure the electron’s energy deposition, followed by hadronic calorimeters used to veto particles giving rise to significant hadronic activity. The electron trigger design, the algorithm for electron reconstruction and the electron identification criteria are described in this section.
\\
  
L1 trigger uses reduced gran- ularity signals covering ∆η × ∆φ ∼ 0.1 × 0.1 (trigger towers) from the calorimeters to identify the positions of RoIs and compute the transverse energy ET of electromagnetic clusters with a precision of 1 GeV. For each trigger tower, the EM or hadronic calorimeter cells (without the fourth layer of the hadronic endcap and barrel-endcap gap scintillators) are summed. EM-clusters are formed by identifying local maxima using a sliding window algorithm based on a 4 × 4 group of trigger towers. A trigger is satisfied if the window’s core-region, the central 2 × 2 trigger towers, contains one pair of neighboring towers with a combined energy above the threshold.
 \\
 
 At L2, the EM calorimeter algorithms build cell clusters within the RoI identified by the L1. Due to latency constraints, the L2 algorithm uses only the second layer of the EM calorimeter to find the cell with the largest deposited transverse energy in the RoI close to the L1 position. This cell is called the pre-seed. The final cluster position is obtained by calculating the energy weighted average cell positions on a 3 × 7 grid centered on the pre-seed. In order to accumulate the energy, two cluster sizes are used, following the same policy used by the offline electron reconstruction: 3 × 7 (∆η × ∆φ ∼ 0.075 × 0.175) cells grid when the cluster is the barrel (|η| < 1.4) and 5 × 5 (∆η × ∆φ ∼ 0.125 × 0.125) when the cluster is in the end-cap (1.4 < |η| < 2.47).
 \\
 
 At the EF, offline-like algorithms are used for the reconstruction of calorimeter quantities. After retrieving the cell information from a region slightly larger than the RoI, the EF uses the offline sliding window algorithm to build the cluster and apply all the offline based corrections.
\\

The standard offline selection defines three operating points for electrons (loose, medium, tight) at increasing levels of background rejection. The variables used for offline electron identification at each identification operating point are described in [91]. A re- optimized menu was defined for electron selection, referred to as the plus-plus menu, providing three additional operating points (loose++, medium++, tight++) with improved performance over the standard menu for a higher pileup environment. In the plus-plus re-optimized identification, more variables are used and the cut values of each operating point are varied. 
\\

The electron reconstruction algorithm used in the central region (|η| < 2.5) of the detector equipped with the ID identifies energy deposits in the EM calorimeter and asso- ciates these clusters of energy with reconstructed tracks in the ID. The three-step process is as follows.
\\

Cluster reconstruction: EM clusters are seeded from energy deposits with total transverse energy above 2.5 GeV by using a sliding-window algorithm with window size 3 × 5.
\\

Track association with the cluster: Within the tracking volume, tracks with pT > 0.5 GeV are extrapolated from their last measured point to the middle layer of the EM calorimeter. The extrapolated η and φ coordinates of the impact point are compared to a corresponding seed cluster position in that layer. They are matched if the distance satisfies |∆η| < 0.05 and |∆φ| < 0.1. An electron candidate is considered to be reconstructed if at least one track is matched to the seed cluster. In the absence of a matching track, the cluster is classified as an unconverted photon candidate.
\\

Reconstructed electron candidate: After a successful track–cluster matching, the cluster sizes are optimized to take into account the overall energy distributions in the different regions of the calorimeter. In the EM barrel region, the energy of the electron cluster is collected by enlarging its size to 3 × 7, while in the EM endcaps the size is increased to 5 × 5. The total reconstructed electron candidate energy is determined from the sum of four contributions: the estimated energy deposit in the material in front of the EM calorimeter; the measured energy deposit in the cluster, corrected for the estimated fraction of energy measured by the sampling calorimeter; the estimated energy deposit outside the cluster (lateral leakage); and the estimated energy deposit beyond the EM calorimeter (longitudinal leakage). The (η, φ) spatial coordinates of the electron candidate are taken from the parameters of the matched track at the interaction vertex. The absolute energy scale and the intercalibration of the different parts of the EM calorimeter are determined using tightly selected electrons from Z → ee, J/ψ → ee and W → eν decays [94], which are discussed later in this section.
\\

In the forward region (2.5 < |η| < 4.9), where there is no tracking detectors, the
electron reconstruction uses only the information from the EMEC and forward calorime- ters and therefore no distinction is possible between electrons and photons. Due to the reduced detector information in this region, the use of forward electrons in physics anal- yses is restricted to the range ET > 20 GeV. In contrast to the fixedsize sliding-window clustering used in the central region, the forward region uses a topological clustering algorithm. The direction of the forward-electron candidates is defined by the barycenter of the cells belonging to the cluster.
\\


\subsection{Muon}
Muons in the final state are a distinctive signatures of many physics studies performed using collisions of high energy protons at the LHC. High-performance of muon trigger, reconstruction and identification is essential. The ATLAS muon system is designed to measure muons in a wide momentum range with high efficiency.
\\

The L1 muon trigger uses the spatial and temporal coincidence of hits either in the RPC or TGC trigger chambers pointing to the beam interaction region. The degree of deviation from the hit pattern expected for a muon with infinite momentum is used to estimate the pT of the muon with six possible thresholds. The number of muon candidates passing each threshold is used in the conditions for the global L1 trigger. Following a global trigger, the pT thresholds and the corresponding detector regions (RoIs) are then sent to the HLT for further consideration.
\\

The L2 uses refined pT measurements and exploits the full granularity of the detector within the RoIs. Two algorithms are used at L2: L2 SA muon using the data from the MDT chambers, and L2 CB muon that combines the L2 SA muon with a track found in the ID. To achieve the needed resolution in sufficiently short time, the pT of the L2 SA muon is reconstructed with simple parametrized functions. The L2 CB algorithm selects the closest IDtrack in the η−φ planes as the best matching track , and the pT is calculated as the weighted average of the L2 SA muon and the ID track (L2 CB muon).
\\

Muon reconstruction and identification in ATLAS uses independent track reconstruction in the ID and MS, which are then combined. Track reconstruction in the MS is logically subdivided into the following stages: preprocessing of raw data to form drift- circles in the MDTs or clusters in the CSCs and the trigger chambers, pattern-finding and segment-making, segment-combining, and finally track-fitting. Track segments are defined as straight lines in a single MDT or CSC station. The search for segments is seeded by a reconstructed pattern of drift circles and/or clusters. There are four classes of reconstructed muons used in ATLAS.
\\

Stand-alone (SA) muon: the muon trajectory is reconstructed only in the MS. The direction of flight and the impact parameter of the muon at the interaction point are determined by extrapolating the spectrometer track back to the point of closest approach to the beam line, taking into account the energy loss of the muon in the calorimeters. SA muons are mainly used to extend the acceptance to the range 2.5 < |η| < 2.7 which is not covered by the ID;
\\

Combined (CB) muon: track reconstruction is performed independently in the ID and MS, and a combined track is formed from the successful combination of a SA track with an ID track;
\\

Segment-tagged (ST) muon: a track in the ID is identified as a muon if the track, extrapolated to the MS, is associated with at least one segment in the precision muon chambers;
 \\
 
Calorimeter-tagged (CaloTag) muons: a track in the ID is identified as a muon if it could be associated to an energy deposit in the calorimeter compatible with a minimum ionizing particle. This type has the lowest purity of all the muon types but it recovers acceptance in the uninstrumented regions of the MS (|η| < 0.1). Only muons with the momentum range of 25 􏰦 < pT <􏰦 100 GeV are considered for this class.
 \\
 
The misalignment of the muon chambers causes a deviation of the measured from the predicted shape of the invariant dimuon mass spectrum. In order to match the Monte Carlo prediction with the experimental measurement, the reconstructed muon momenta from MC must be smeared and shifted. The momentum resolution and scale of the are calibrated using Z → μμ and J/ψ → μμ decays.
\\
 
 \subsection{Jet}

Jets are the dominant feature of high-energy, hard proton–proton interactions at the LHC. They are key signatures of many physics measurements and for searches for new phenomena. In ATLAS, jets are observed as groups of topologically related energy de- posits in the calorimeters, associated with tracks of charged particles measured in the inner tacking detector. They are reconstructed with the anti-kt jet algorithm [102] and are calibrated using Monte Carlo(MC) simulation.

Jets are reconstructed using the anti-kt algorithm with distance parameters R = 0.4 or R = 0.6, utilizing the FastJet software package [104]. The four-momentum scheme is used at each recombination step in the jet clustering. The total jet four-momentum is therefore defined as the sum of the four-momenta of all its constituents. Four types of jet are reconstructed in ATLAS:
\begin{itemize}
\item truth jets. The inputs to the jet algorithm are stable simulated particles;
\item track jets. The inputs are reconstructed tracks in the ID;
\item calorimeter jets. The inputs are energy deposits in the calorimeter;
\item TCC jets. The inputs are energy deposits in the calorimeter, with corrections by track information;
\end{itemize}
A schematic overview of the ATLAS jet reconstruction is presented in Figure~\ref{fig:recostruction_jets}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/reconstruction_simulation/jets.pdf}
  \caption {Cut-away view of the ATLAS inner detector. \cite{Aad:2008zzm}.
  }. 
  \label{fig:recostruction_jets}
\end{figure}

Topological clusters (topocluster) are groups of calorimeter cells that are designed to
follow the shower development taking advantage of the fine segmentation of the ATLAS

  \subsection{MET}

\section{MC simulation}
  \subsection{basic theory}
  \subsection{Genarators and Parton Shower Tools}
