\subsection{Inner Detector}
\label{sec:inner_detector}

The Inner Detector (ID) \cite{Aad:2008zzm} is the innermost section of the ATLAS detector, 
closest to the interaction point, 
surrounded by the Central Solenoid Magnet with $2$ T axial magnetic field. 
The detector is therefore designed to provide robust pattern recognition and excellent momentum
 resolution. It also provides both primary and secondary vertex measurements for charged tracks
  in an area above a given $p_T$ threshold (nominally $0.5$ GeV) and within the range $|\eta| < 2.5$.
 Electron identification within $|\eta| < 2.0$ can also been provided by the ID within an energy 
 range from $0.5$ GeV to $150$ GeV. \\

Layout of the ID is shown in Figure~\ref{fig:inner_detector}.  ID is composed of three subsystems:
the Pixel Detector, the Semiconductor Tracker (SCT), and the Transition Radiation Tracker (TRT), 
from the inside out. 
The discrete space-points of Pixel and the stereo pairs of SCT provide high-resolution 
pattern recognition capabilities in the high radiation range. In the outer layer, TRT provides 
continuous tracking to improve the pattern recognition and momentum resolution in both
$R-\phi$ and $z$ coordinates . 
Figure~\ref{fig:id_sensors} shows the ID sensors and structural elements in barrel and endcap
regions.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/experiment/inner_detector.jpg}
  \caption {Cut-away view of the ATLAS inner detector, showing the alignment of the Pixel Detector,
  the SCT and the TRT \cite{Pequenao:1095926}.
  }
  \label{fig:inner_detector}
\end{figure}


\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/experiment/inner_detector2.jpg} \\
   \includegraphics[width=1\textwidth]{figures/experiment/inner_detector3.png}
  \caption {Showing the inner detector sensors and structural elements traversed by 
  charged tracks in the barrel (top) and endcap (bottom) regions \cite{Pequenao:1095926} 
  \cite{Kayl:2010db}.
  }
  \label{fig:id_sensors}
\end{figure}


\paragraph{Pixel detector}

The pixel detector system \cite{Aad:2008zz} is the innermost element of the Inner Detector.
The detector provides excellent spatial resolution for reconstructing primary vertices, as well 
as the identification and reconstruction of secondary vertices. The system composed of three 
barrel layers and two identical endcap regions, each contains three disk layers, as shown 
in figure Figure~\ref{fig:id_sensors}, to guarantee at least three pixel hits over the full 
rapidity range.
In addition, a new layer called the Insertable B-Layers (IBL) \cite{LaRosa:2016nbd} 
was installed between the first pixel layer and a new radius beam pipe, 
during the first long shut down. It is now the innermost layer of the pixel detector.
Because of the expected radiation damage, the IBL is designed to maintain or improve the 
ATLAS performance. It is also motivated by the increasing bandwidth requirements by the 
expected Phase-I LHC peak luminosity .
\\

The detector is built with 1456 models in the barrel region and 288 in the two endcaps,
with a total sensitive area of ${\sim}$1.7 $m^2$. 
The model, composed of silicon sensors, front-end electronics and control circuits are 
identical at the sensor/integrated circuit level, in barrel and endcaps regions. However, 
the interconnection schemes are different. 
Each sensor tile consists of $47,232$ pixels with sizes of $50 \times 400$~$\mu$m$^2$ ($88.9$\%)
or $50 \times 600$~$\mu$m$^2$  ($11.1$\%). The detector is therefore contains ${\sim}80$ million 
channels. In addition, the IBL provides ${\sim} 6$ million pixels of $50 \times 250$~$\mu$m$^2$.
During Run 1, the pixel performed very well with tracking efficiency of $99\%$ and spatial
resolution of ${\approx} 8$~$\mu$m in $R-\phi$ and  75 $\mu$m in $z$ \cite{Pernegger:2015qqa}. 
The additional IBL pixels with smaller size are expected to improve the efficiency and resolutions
further. 
\\

\paragraph{Silicon strip detector}

The SCT lies in the middle layer of the inner detector, covering radial distances between 
299 mm and 560 mm.
It is consists of four concentric barrels and in total 18 disks in the two endcaps, including 4088 models.
The modules cover a surface of 63 m$^2$ of silicon and provide almost hermetic coverage with 
at least four precision space-point measurements over the fiducial coverage of the inner detector 
\cite{Aad:2008zzm}. A barrel model consists of two 12 cm-long strips approximately parallel to 
the magnetic filed and beam axis. Each strip contains two 6 cm-long daisy-chained micro-strip sensors, 
with a pitch of 80 $\mu$m. The two strips are glued back-to-back at a stereo angle of 40 mrad to 
measure space point. A endcap disk consists of up to three rings of models with trapezoidal sensors. 
The strip direction is radial with constant azimuth and a mean pitch of 80 μm \cite{Aad:2014mta}. The 
strips in the inner rings have only one sensor each side. The SCT provides an intrinsic hit efficiency of 
$99.74 \pm 0.04 $ \% \cite{Aad:2014mta}. 
\\


\paragraph{Transition radiation tracker}

The transition radiation tracker (TRT) \cite{collaboration_2008} is the outer layer of the inner detector,
covering radial distances from $563$ mm to $1066$ mm and a rapidity range $|\eta| < 2$ . 
It contains many layers of gaseous straw tube elements, in the barrel and two endcaps 
(see Figure~\ref{fig:id_sensors}). $52,544$ straws are aligned parallel with the beam axis in the barrel region. 
In each of the two endcaps, $122,880$ straws are aligned perpendicular to the beam axis and point 
outwards in the radial direction. The straws are filled with a gas mixture of $70$\% Xe, $27$\% 
CO\textsubscript{2} and $3$\% of O\textsubscript{2} is used. 
During Run2, in the models with large gas leakage, Ar is filled instead of Xe,
while other gas components remain unchanged. In a TRT performance study of Run 1, the straw efficiency 
is found to be larger than $96$\% and the straw track position measurement accuracy in the straws better 
than 130 μm in low occupancy conditions \cite{Aaboud:2017odu}.
\\

The combination of the Pixel Detector, the SCT and the TRT gives very robust pattern recognition and high 
precision in both $R-\phi$ and $z$ coordinates. The tracking measurements provided by the ID matched 
by the precision measurements of the electromagnetic calorimeter, enhanced the electron identification
capabilities, heavy-flavor and $\tau$-lepton tagging, secondary vertex measurement performance. 
\\

