\subsection{Muon Spectrometer}
\label{sec:muons}

The muon spectrometer is the outermost layer of the ATLAS detector. Muons emit almost no 
bremsstrahlung due to the their large mass. Since muons are leptons, they do not participate
strong interaction.
As a result, they can not be stopped by the calorimeters. The muon system is designed to measure 
the momentum of muons passing the spectrometer, in the region of $|\eta| < 2.7$. It is 
also designed to trigger muons with $|\eta| < 2.4$.
A view of the spectrometer is shown in Figure~\ref{fig:muons}.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/experiment/muons.jpg}
  \caption {Cut-away view of the ATLAS muon spectrometer. \cite{Pequenao:1095929}.
  }
  \label{fig:muons}
\end{figure}

\paragraph{The Toroid Magnets}

A key point in the design of muon system is the magnetic deflection of muon tracks, 
powered by three large superconducting air-core toroid magnets.
The barrel toroid that is composed of eight coils, bends the muons tracks over the 
pseudorapidity range $|\eta| < 1.4$. The two endcap toroids, each with eight racetrack-like 
coils, are inserted in the barrel toroid at each end, bending tracks over the range 
$1.6 < |\eta| < 2.7$. In the transition region, where $1.4 < |\eta| < 1.6$, the deflection is 
provided by the combination of the barrel and endcap magnetic fields.  
This magnet configuration provides a field which is mostly orthogonal to the muon 
trajectories, while minimizing the degradation of resolution due to multiple 
scattering \cite{Aad:2008zzm}.
 \\

 
 
\paragraph{Muon Chambers}
 
There are four types of chambers in the muons spectrometer, designed for different 
environments and requirements. Precision tracking chambers measure muon tracks 
in the region of $|\eta| < 2.7$. Trigger chambers provide fast information on muon
tracks in the range $|\eta| < 2.4$, over the full $\phi$-range.
 \\
 
 Monitored Drift Tube chambers (MDT's) provide the precision momentum measurement,
 with a pseudorapidity coverage of $|\eta| < 2.7$, except in the innermost end-cap layer 
 where their coverage is limited to $|\eta| < 2.0$. MDT chambers are composed of three 
 to eight layers of drift tubes, which have a diameter of $29.970$ mm, operating with 
 Ar/CO\textsubscript{2} gas ($93$/$7$) at $3$ bar.
 MDT's have an average resolution of 80 $\mu$m per tube, or ${\sim}$35 $\mu$m per 
 chamber \cite{Aad:2008zzm}.  
 \\
 
 Cathode-Strip Chambers (CSC's) are used in the innermost tracking layer, with a coverage
 of pseudorapidity over  $2.0 < |\eta| < 2.7$, namely the forward region. These chambers
 are motivated by the high radiation environment and the requirement of time resolution.
 The CSC's provide high spatial, time and double track resolution with high-rate capability. 
 They are multiwire proportional chambers with cathodes segmented into strips.
 The resolution of a chamber is 40~$\mu$m in the bending plane and about 5 mm in 
 the transverse plane\cite{Aad:2008zzm}.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/experiment/muon_trigger.png}
  \caption {schematics of muon trigger system \cite{Aad:2008zzm}.
  }
  \label{fig:muon_trigger}
\end{figure}

Resistive plate chambers (RPC's) trigger muons in the barrel region ($|\eta| < 1.05$),
due to good spatial and time resolution as well as adequate rate capability. RPC's 
is composed of three concentric cylindrical layers around the beam axis, 
referred to as the three trigger stations, as illustrated in Figure~\ref{fig:muon_trigger}.
Each station consists of two detector layers, each measuring $\eta$ and $\phi$.
A low-$p_T$ trigger requires only RPC1 and PRC2, while all three stations 
are used to create the high-$p_T$ trigger.
\\


Thin gap chambers (TGC's) provide two functions: muon trigger capability in the 
endcap region ($1.05 < |\eta| < 2.4$) and the complement of the measurement
of the MDT's in the radial direction. TGC's operate on the same principle 
as multi-wire proportional chambers, providing good time resolution and high rate 
capability. As shown in Figure~\ref{fig:muon_trigger}, the trigger detectors are mounted
in two concentric rings, an outer one covering the range $1.05 < |\eta| < 1.92$, while
an inner one covering the range $1.92 < |\eta| < 2.4$.  
% \textcolor{red}{double check the inner wheel $|\eta|$ coverage, current numbers are taken from \cite{Aad:2008zzm} } 
\\