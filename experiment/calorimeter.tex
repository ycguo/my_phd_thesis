\subsection{Calorimeter}
\label{sec:calorimeter}

The ATLAS calorimeter system \cite{Aad:2008zzm} consists of several sampling calorimeters, 
covering $|\eta| < 4.9$ with full $\phi$-symmetry around the beam axis. The system deposits 
the energy of electrons, photons, muons, jets, and provides a measurement of $\MET$. 
% footnote for muons: energy of muons is not fully deposited. 
A sampling calorimeter is composed of alternating layers of passive absorbers and active 
detectors. The interaction between particles and the dense passive material, leads to 
electromagnetic or hadronic showers. The energy deposits are then measured in the 
active layers. 
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/experiment/calorimeter.jpg}
  \caption {Cut-away view of the ATLAS calorimeter system \cite{Pequenao:1095927}.
  }
  \label{fig:calorimeter}
\end{figure}

A view of the calorimeters is presented in Figure~\ref{fig:calorimeter}. Two different 
sampling techniques are chosen due to the varying requirements of physics processes 
and the radiation environment. Liquid argon (LAr) is used as the active detector medium
in the calorimeters closest to the beam-line, including an electromagnetic calorimeter in the 
barrel, an electromagnetic endcap calorimeter (EMEC), a hadronic endcap calorimeter (HEC) 
and a forward calorimeter (FCal)  in each endcap. The passive absorber medium is not
 uniformed in these calorimeters.
Scintillator tiles are applied in the outer hadronic calorimeter, 
with steel used as the absorber medium. According to physics goals, the system can be 
divided in to the electromagnetic calorimeter and the hadronic calorimeter.
\\


\paragraph{Electromagnetic Calorimeter}

The electromagnetic (EM) calorimeter measures the energy of electrons and photons.
It is housed in three cryostats, a barrel calorimeter ($|\eta| < 1.475$) and two endcaps 
($1.375 < |\eta| < 3.2$).
The barrel calorimeter is separated by a small gap ($4$ mm) at $z = 0$. Two identical half-barrels
lie at the two sides of the gap. Each endcap include a inner wheel covering the pseudorapidity region
$2.5 < |\eta| < 3.2$ and an outer wheel covering $1.375 < |\eta| < 2.5$. In order to get a full $\phi$ 
coverage without any azimuthal cracks, an accordion geometry is chosen for the absorbers and the 
electrodes of both barrel and endcaps (see Figure~\ref{fig:emcal_geometry}). Such a geometry also 
provides a fast extraction of signal.
The accordion waves are axial and run in $\phi$ in the barrel calorimeter, and are radial and run
 axially in endcap calorimeters. The arrangement leads to a very uniform performance in terms of 
 linearity and resolution as a function of $\phi$ \cite{Aad:2008zzm}.
 \\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/experiment/calorimeter2.png}
  \caption {Showing the accordion geometry of the ALTAS EM Calorimeter\cite{Aad:2008zzm}. 
  }
  \label{fig:emcal_geometry}
\end{figure}

The EM calorimeter is segmented in three sections in depth (see Figure~\ref{fig:emcal_geometry})
over the region $|\eta| < 2.5$, which is devoted to precision measurements.
The first layer is designed provide shower shape measurements with finest granularity in $\phi$. 
The second layer collects the largest fraction of energy of the EM shower since it occupied most 
The third layer has the coarsest segmentation.
For the endcap inner wheel, the calorimeter is divided in two sections in depth and therefore has a 
coarser lateral granularity. 
These layers provide a total thickness $> 22 $ interaction length ($X_0$) in the barrel and 
$ > 24 X_0$ in the endcaps \cite{Aad:2008zzm}.
\\


\paragraph{Hadronic calorimeter}

The ATLAS hadronic calorimeters are designed to measure the energy of jets. 
It is composed of three parts: a tile calorimeter, LAr Hadronic Endcap Calorimeters (HEC), and
LAr Forward Calorimeters (FCal).
\\

The tile calorimeter is located outside the electromagnetic calorimeter, with a inner radius of $2.28$ m
and an outer radius of $4.25$ m. It has a barrel section covers $|\eta| < 1.0$, and two extended barrels 
covers the region $0.8 < |\eta| < 1.7$. The three barrels are divided azimuthally into 64 models, each with
$\phi \approx 0.1$ and segmented in three layers, radially. The three layers are about $1.5$, $4.1$, and 
$1.8$ interaction length ($\lambda$) in the barrel and $1.5$, $2.6$, and $3.3 \lambda$ in extended barrels. 
The total thickness at the outer edge of the tile-instrumented region is $9.7 \lambda$ at $\eta = 0$ 
\cite{Aad:2008zzm}. The TileCal contributed to high-quality ATLAS data taking with an efficiency 
higher than $99$\% during all three years of Run 1, with good stability of the calorimeter response in time 
\cite{Aaboud2018:tile} . 
\\

The Hadronic Endcap Calorimeters (HEC) \cite{Gingrich_2007} is composed of two endcaps, each
with two independent wheels, placed right behind the endcap EM calorimeter. 
The HEC is integrated into the same cryostats with the endcap EM calorimeter.
The HEC extend out the $|\eta| = 3.2$ to avoid the drop in material density at the transition
between the forward calorimeter around $|\eta| = 3.1$. For the same reason, the HEC $|\eta|$ coverage is
also slightly overlap with the tile calorimeter by extending to $|\eta| = 1.5$. Each wheel is made of $32$
identical wedge-shaped modules and segmented into two layers in depth. The inner radius is $0.475$ m,
except the overlap region with the FCal, where the radius becomes $0.375$ m. The outer radius is $2.03$ m.
The wheels are built from cooper plates, as absorb medium, interleaved with LAr gaps.
\\

The Forward Calorimeters (FCal) shares the same cryostats with the HEC. The FCal provides a coverage
over $3.1 < |\eta| < 4.9$. The FCal is designed with high-density materials with a depth of ${\sim}10 \lambda$.
It is consists of three models, each with a radius of $0.455$ m and a thickness of $0.45$ m. 
The first model optimized for EM measurement is made of cooper. 
The other two, measuring the energy of hadronic interactions, are made of tungsten. 
\\

The hadronic calorimeters with thicknesses of ${\sim}9.7 \lambda$ in the barrel and $10 \lambda$ in the 
endcaps provide good resolution for high energy jets\cite{Aad:2008zzm}.
\\