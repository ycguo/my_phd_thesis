\section{The Large Hadron Collider}
The Large Hadron Collider (LHC) is the world's largest and most powerful particle accelerator and collider. 
It is a two-ring-superconducting-hadron accelerator and collider with a circumference of $26.7$ km, lying 
between $45$ m and $170$ m underground \cite{Evans:2008zzb}, housed at the European Organization 
for Nuclear Research (CERN) near Geneva, Switzerland.  
The machine mainly designed for proton-proton collisions, with a maximum center of mass energy of
 $14$ TeV. During its first run period (Run 1), the center of mass energy has been $7$ TeV (2010-2011) 
 and $8$ TeV (2012). After a long shut down (2013-2015), the center of mass energy was increased to 
 $13$ TeV and remained unchanged during Run 2 (2015-2018).  
\\

As illustrated in Figure~\ref{fig:lhc_acc_complex},  the LHC is the last element in the accelerator complex 
at CERN \cite{cernweb:acc}. Hydrogen atoms with their electrons stripped off, are fed in to the Linear 
accelerator 2 (LINAC2). The protons are accelerated to the energy of $50$ MeV in LINAC2 and are 
injected into the Proton Synchrotron Booster (PSB), that pushes them to 1.4 GeV. They are then sent
to the Proton Synchrotron (PS, 25 GeV) and the Super Proton Synchrotron (SPS, 450 GeV). The protons
are finally transferred to the two beams pipes of LHC, circulating in opposite directions.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=1\textwidth]{figures/experiment/acc_complex.pdf}
  \caption {The CERN accelerators complex. The LHC is the last ring (dark blue line) in a complex chain of 
  particle accelerators. The smaller machines are used in a chain to help boost the particles to their final 
  energies \cite{Mobs:2197559}.
  }
  \label{fig:lhc_acc_complex}
\end{figure}


\subsection{The Experiments}

There are four main detectors on the ring of the LHC, shown in Figure~\ref{fig:lhc_acc_complex}.
\\

\begin{itemize}
\item ATLAS and CMS. ATLAS (A Toroidal LHC ApparatuS) \cite{Aad:2008zzm} and CMS 
(Compact Muon Solenoid) \cite{Chatrchyan:2008aa} are both general purpose detectors. They are 
designed to study a wide range of physics interests, from the Standard Model precise measurements to 
searches for new physics phenomena. 
\\
\item LHCb \cite{Alves:2008zz} is dedicated to heavy flavor physics. The primary goal of this experiment is to 
look for indirect evidence  of new physics in CP violation and rare decays of beauty and charm hadrons. 
To explain the amount of matter in the universe, a new source of CP violation beyond the SM is needed. 
As predicted by many models, such a new source may change the expectations of CP violating phases, rare
decay branching ratios, and generate decay modes not allowed by the SM. LHCb is aimed to examine these
possibilities.
\\
\item ALICE (A Large Ion Collider Experiment) \cite{Aamodt:2008zz} is a general purpose, heavy-ion detector 
focuses on the strong-interaction. 
Different from the detectors mentioned above, this detector, running with nucleus-nucleus collisions, is designed
to address the physics of strongly interacting matter and the quark-gluon plasma at extremely high energy density 
and temperature. ALICE will also taking data during proton-proton runs as reference data for the heavy-ion
program and to address specific strong-interaction topics as complementary to other LHC detectors. 
\end{itemize}


\subsection {Proton-Proton Collision}
After the final injection and acceleration in the LHC, the two beams of protons are brought into collision inside
the four detectors. Each beam consists of a large number of bunches, with ${\sim}10^{11}$ protons each, and a 
bunch spacing of $25$ ns. Each bunch crossing produces multiple interactions, with the average number of 
interactions per bunch crossing defined as ${<}\mu{>}$. Figure~\ref{fig:lhc_mu} shows ${<}\mu{>}$  in the ATLAS 
detector during data taking periods in Run 2.
\\

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/experiment/mu_2015_2018.png}
  \caption {Average number of interactions per bunch crossing ${<}\mu{>}$ in the ATLAS detector during the data 
  taking periods in Run2 \cite{cernweb:lumiResult}.
  }
  \label{fig:lhc_mu}
\end{figure}


For a given physics process, the proton-proton collision event rate is given by:
\begin{equation}
N = L \sigma
\end{equation}

where $\sigma$ is the cross section for the physics process and $L$ is the luminosity. The luminosity 
depends only on beam parameters and can be written for a Gaussian beam profile as \cite{Evans:2008zzb},
\begin{equation}
L = \frac{N^2_b n_b f_r \gamma}{4 \pi \epsilon \beta^{\star}} F
\end{equation}
\\
where $N_b$ is the number of particles per bunch, $n_b$ the number of bunches per beam, $f_r$ the revolution 
frequency, $\gamma$ the relativistic $gamma$ factor,  $\epsilon_n$ the normalized transverse beam emittance,
$\beta^{\star}$ the $\beta$ function at the collision point, and F the geometric luminosity reduction factor due to 
the crossing angle at the interaction point (IP),
\begin{equation}
F = \frac{1}{\sqrt{1 + \frac{\theta_c \sigma_z}{2 \sigma^{\star}}}}
\end{equation}
where $\theta_c$ is the the full crossing angle at the interaction point (IP), $\sigma_z$ the rms bunch length and 
$\sigma^{\star}$ the transverse rms beam size at the IP. The reduction factor F reduces the luminosity by about 
15\%.
\\

The integrated luminosity of proton-proton collision delivered by the LHC and recorded by the ATLAS detector are 
shown in Figure~\ref{fig:lhc_intlumi}.
\\

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.45\textwidth]{figures/experiment/intlumivsyear.png}}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{figures/experiment/intlumivstimeRun2DQall.png}}
  \caption{ (a) shows cumulative integrated luminosity delivered by the LHC each year during Run 1 and Run 2; 
  (b) shows integrated luminosity delivered by the LHC (green), recorded by the ATLAS experiment (yellow), and
  good for physics analyses (blue) during Run 2\cite{cernweb:lumiResult}.
  }
  \label{fig:lhc_intlumi}
\end{figure}
