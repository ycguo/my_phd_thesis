\section{Event Selection and Categorization}
\label{sec:dm_selection}

\subsection{Event Preselection}
\label{sec:dm_preselection}

The event level preselection for mono-$V$ analysis are same as the $ZV$ resonance search, including data clean, jet clean, 
and \MET\ trigger requirements, see Section~\ref{sec:zv_evpreselection}. 
After event level preselection, events are split into merged and resolved regimes, to reconstruct  the highly boosted and 
resolved events, respectively. As mentioned in Section~\ref{sec:dm_intro}, in the merged regime the decay products of 
the $W/Z$ boson are reconstructed as a single large-$R$ jet, while in the resolved regime the two jets are reconstructed as individual small-$R$ jets.

A recycling event selection strategy is applied to ensure no overlap between these two regimes. The selection is as follows:
\begin{itemize}
	\item For $250 \,\text{GeV} < {E}^{\text{miss}}_{\text{T}}$:
	\begin{enumerate}
		\item If an event passes the event selection for the merged topology, it is accepted in the merged regime.
		\item If an event fails the event selection for the merged topology, it is checked whether it satisfies for the resolved event selection and could be accepted in the resolved regime.
	\end{enumerate}
	\item For $150 \,\text{GeV} < {E}^{\text{miss}}_{\text{T}} < 250 \,\text{GeV}$:
	\begin{enumerate}
		\item If an event passes the event selection for the resolved topology, it is accepted in the resolved regime.\\
	\end{enumerate}
\end{itemize}

Study has shown that in the lower mass region, where $150 \,\text{GeV} < {E}^{\text{miss}}_{\text{T}} < 250 \,\text{GeV}$, 
no significant gain in sensitivity can be expected by recycling the events failed resolve regime selection (into the merged regime).
\\

After the above selection, events are required to have no loose electrons or muons,
and \MET\ greater than 150 GeV or 250GeV for resolved and merged analysis respectively.

After these requirements, there are still a large number of events coming from a dijet topology 
in which one of the jets is poorly measured, causing an imbalance and therefore high \MET.  
These type of events are removed by imposing requirements on the following three event level quantities,
referred to as ``anti-QCD'' cuts throughout the note. 
%Some distributions of these cuts are shown in ~\ref{fig:dm_antiqcdcuts_nocut}.
\begin{itemize}
\item \MPT\ $>$ 30 GeV or presence of two or more b-tagged jets (large-$R$ jets for merged, central jets for resolved): 
Even if a jet in a dijet event is mismeasured in the calorimeter, the tracks will still be well-reconstructed.
Therefore, they will balance to a greater extent, and the overall track-based MET will be at lower values when the \MET\
is generated by fluctuations in the charged to neutral fractions of the jets in such events.

\item min($\Delta\Phi$(\MET, small-R jets)) > 20\degr : If a jet is mismeasured, the \MET\ will be collinear with
the mismeasured jet. This can be most clearly understood with a dijet topology, where one jet is measured with very large energy.
Therefore this variable is expected to be relatively small for events with fake \MET.
%Since there is no true source of \MET\ in dijet events (other than from $b$ decays)
%dijet events should have low \MET.
Note that in the calculation of this observable, when no small-R jets are found in the event based on the criteria in Section~\ref{sec:dm_objects}, 
the event is assigned the default value of $\pi$. This is by far the most dominant cut to reduce the QCD contribution of mismeasured multijet events.

\item $\Delta\Phi$ (\MET, \MPT) $<$ 90\degr : In the case of an event with real \MET, \metvec should be aligned with \mptvec 
as they both are representative of  the direction of undetected energy flow, whether it be measured in the tracker or the calorimeter.  
However, in the case of a dijet event with a mismeasured jet, \metvec and \mptvec will each align with one of the two jets (they may not align to the same jet).  
Therefore, the observable will either reside at extremely high or low values.  
After selecting on the other two observables, it is observed that the events are biased to reside primarily at high values.

\item $\Delta\Phi$ (\MET, J/ jj) $>$ 120\degr : Following the same logic, for an event with dark matter recoiling against a vector boson 
which is reconstructed as large-$R$ jet (J) or 2 small-$R$ jets (jj),  the azimuthal angular distance should be large in a back-to-back collision.
\end{itemize}

These selection criteria, chosen to remove a large fraction of QCD multijet events, are based on the physical intuition previously
described.  The exact selection values are chosen by examining the spectra of
these observables and removing regions in which there is poor modeling of the data by
the backgrounds that are expected to be dominant in the topology of interest(\Zjets, \Wjets, $t\bar{t}$).  No extensive optimization
for these selections was performed.



\subsection{Signal region selection}
\label{sec:dm_srvalidation}

On top of the event preselections, the following cuts are applied based on signal topology,
\begin{itemize}
    \item require at least one fat jet with $\pt>200$ GeV and $|\eta|<2.0$ for the merged selection 
    \textbf{OR} require at least two central small-$R$ jets for the resolved selection

    \item categorize the event in \textbf{b-tagging regions} based on the number of b-tagged track-jets in the merged selection 
    \textbf{OR} the number of b-tagged central jets in the resolved selection.

    \item In the resolved selection, require the two leading central small-$R$ jets to satisfy $\Delta \phi (j_1, j_2) < 140 ^{\circ}$ and
    $\Delta R (j_1, j_2) < 1.4 (1.25)$ for 0 or 1 (2) b-tags. 
    In the merged selection  require that the leading jet $p_{\text{T}} > 45$ GeV, $\sum_j p_{\text{T}}^{j} > 120$ \rm GeV for 2 jets or 150 GeV for 3 or more jets.\\

\end{itemize}
Different signal jet tagging is applied in different b-tagging regions.
Events in merged regime with 0 or 1 b-tagged track jet are required to pass the smoothWZTagger
which composed of a mass window and a \DTwoBetaOne substructure selection, as discussed in Section~\ref{sec:zv_largerjets}.
The events pass the both mass and \DTwoBetaOne requirements are put in high-purity region. 
Other events pass only mass requirements of the $W/Z$ tagger are put in low-purity region.
Events in merged regime with 2 b-tagged track jets and events in resolved region are required to pass mass window cuts.
The signal jet tagging in different region are listed below:
\begin{itemize}
	\item merged 0/1 b-tag high-purity regions: require the leading large-R jet passing  $W$ or $Z$ tagger (both mass and \DTwoBetaOne substructure requirements).
	\item merged 0/1 b-tag low-purity regions: for events not in the high-purity region, require the leading large-R jet passing  $W$ or $Z$ tagger mass requirements.
	\item merged 2 b-tag region: require the leading large-$R$ jet to satisfy 75 GeV $ < m_J < 100 $ GeV
	\item resolved 0/1/2 b-tagged regions: require the leading two small-$R$ jets to satisfy 65 GeV $   < m_{jj} < 105 $ GeV
\end{itemize}

A summary of signal region event selection and categorization are shown in Table~\ref{tab:dm_event_sel}.
\\

Figure~\ref{fig:dm_acceptance} shows the signal acceptance $\times$ efficiency after full selection. For each mass point all the regions are all combined. 
The low acceptance in the low mass region is caused by the high \MET\ cut (\MET\ > 150 GeV).
\\

\begin{table}[ht!]
\center
\includegraphics[width=\textwidth]{figures/monoV/monoV_event_sel.pdf}
\caption{A detailed summary of the signal region event selections used in the mono-V analysis\cite{Aaboud:2018xdl}.}
\label{tab:dm_event_sel}
\end{table}

\begin{figure}[ht!]
\centering
\includegraphics[width=0.8\textwidth]{figures/monoV/acceptance/SignalAcceptance.pdf}
\caption{\label{fig:dm_acceptance} Acceptance $\times$ efficiency for the vector mediator model at different mass points, with all regions combined.}
\end{figure}

\subsection{Validation regions}

The mass side-band regions are used as validation regions (VR). Different to the $ZV$ resonances search, the validation regions are 
used in the fitting to constrain background in the mono-$V$ analysis.
The mass side-band regions are defined by the same selection criteria, except for the mass widow. In these regions, 
the leading large-$R$ jet mass or the dijet invariant mass are required to be above the mass window in signal region and 
below 250 GeV. This ensures the similarity of the VR to the SR and the VR is well described by the simulated background MC samples.
\\

