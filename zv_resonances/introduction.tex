\section{Introduction}
\label{sec:zv_introduction}
After the discovery of Higgs boson in 2012 \cite{Aad:2012tfa} \cite{Chatrchyan:2012xdj}, searching for new heavy particles
beyond the Standard Model (BSM) becomes a hot topic at the Large Hadron Collider (LHC). Many BSM models,
aiming to solve the hierarchy problem and naturalness arguments, predict the existence of new heavy resonances that 
can decay into dibosons, for example, the Randall–Sundrum (RS) model with a warped extra dimension 
\cite{PhysRevLett.83.3370} \cite{PhysRevLett.83.4690},
models with extended Higgs sectors as in the two-Higgs-doublet model (2HDM)\cite{Branco:2011iw}, 
models with composite Higgs bosons\cite{Contino:2011np},
and extended gauge sectors as in Grand Unified Theories (GUT) \cite{PhysRevD.10.275} \cite{PhysRevLett.32.438}.
There are also models provide a framework that bridging theories with extra vectors and the experimental data,
 such as the Heavy Vector Triplet Model (HVT) \cite{deBlas:2012qp} \cite{Pappadopulo:2014qza}.
These models are introduced and discussed in Section~\ref{sec:introduction} and Section~\ref{sec:theory_diboson}.
\\

This study focus on heavy resonances X decaying into pairs of vector bosons, $ZV (V = W,Z)$, using LHC Run 2 data. 
Such a resonance maybe a spin-0 Higgs boson (H) which can decay into $ZZ$, a spin-1 $W'$ predicted by the HVT model that 
may decay into a $Z$ boson and a $W$ boson, a spin-2 Kaluza-Klein (KK) excitations of the graviton ($G_{KK}$) 
from RS model which are expected to decay into a pair of $Z$ bosons. Depending on the models, resonances can be
 produced through gluon-gluon fusion (ggF),  vector-boson fusion (VBF) , and Drell-Yan (DY) processes. 
 Feynman diagrams of these precess are shown in Figure~\ref{fig:fm_rsg} and Figure~\ref{fig:fm_hvt} in Section~\ref{sec:theory_diboson}.
 \\
 
In Run 1, searches in this final state signature were performed by the ATLAS collaboration at $\sqrt{s}=$8 TeV in 
the context of searching for a heavy Higgs boson decaying to a pair of \Z bosons~\cite{Aad:2015kna} 
in the $\ell\ell\ell\ell$, $\ell\ell qq$, $\ell\ell\nu\nu$, and $\nu\nu qq$ final states. 
No significant excess was found in these channels. Upper limits on signal production cross-section were set using a simultaneous fit
 to all channels, interpreted in the context of heavy Higgs with a narrow width and type-I and type-II 2HDM.  
%Another search in the \MET+fatjet final state was performed by the ATLAS collaboration, but in the context of
%searching for WIMP dark matter~\cite{Aad:2013oja}.  
%This search was carried out using the full $\sqrt{s}=$8 TeV dataset and no significant deviation from the standard model was observed.  
The CMS collaboration has also performed searches in same final states.  With data collected at $\sqrt{s}=$ 7 TeV, 
a search was performed for SSM $W'$ bosons and Kaluza Klein gravitons which resulted in no significant
deviation from the background expectation with lower limits on the mass of the $W'$ or $G^{*}$ of approximately 900 GeV~\cite{Chatrchyan:2012rva}.
%Another search in this same MET+fatjet final state was performed by the CMS collaboration using the full $\sqrt{s}=$ 8 TeV dataset but in the context
%of searching for a mono-\W/\Z signature of the production of WIMP dark matter~\cite{EXO-12-055}.  Again, no significant deviation from the
%standard model background expectation was observed.
\\

The results of this study using 13 TeV dataset recorded in 2015 and 2016 has been published in 2 ATLAS conference notes and 2 papers:
\begin{itemize}
\item A search for heavy ($m >$ 1 TeV) resonances~\cite{ATLAS-CONF-2015-068}\cite{Aaboud:2016okv} with 2015 data, 
\item A search for heavy $ZZ$ and $ZW$ resonances~\cite{ATLAS-CONF-2016-082} in the $\ell\ell qq$ and $\nu\nu qq$ final states with 13.2 $\textrm{fb}^{-1}$ data.
\item A search for heavy $ZZ$ and $ZW$ resonances~\cite{Aaboud:2017itg} in the $\ell\ell qq$ and $\nu\nu qq$ final states with 36.1 $\textrm{fb}^{-1}$ data.
\end{itemize}
The study with full Run2 data (2015-2018) is still ongoing. This chapter presents the working strategy and results based on the 36.1 $\textrm{fb}^{-1}$ analysis.
\\

In the events that are targeted by this search, the large mass of the new particle causes the gauge bosons to be produced with large momentum.
This leads to three distinguishing aspects of signature. First, due to the presence of neutrinos, this process produces large amounts of missing transverse momentum.
Second, due to the presence of the gauge boson which decays to quarks, this hadronic decay is collimated to the extent that it can be reconstructed as a single, large radius jet.
This topology is optimal for heavy resonances ($m\gtrsim$ 800 GeV) because of the boost of the vector boson. 
Below this mass the efficiency might be not optimal.
The final feature is the presence of a resonant shape near the mass of the new particle. 
Therefore, searching for this resonant feature provides a sensitive handle to the presence of new physics.
The main steps of the analysis are outlined in the following, and are explained in detail in following sections.

\begin{itemize}
\item Select events with a large \MET\ and at least one large radius jet.
\item Select events that identify the jets as originating from a boosted hadronically-decaying vector boson.
A W/Z tagger using jet mass and substructure is applied in this step. The tagger is described in Section~\ref{sec:zv_objects}.
\item Define control regions using selections that give relatively pure samples of events composed of the main backgrounds, 
which include \Zjets, \Wjets, top-quark related processes, and diboson processes.
This is achieved by selecting events with various numbers of leptons, b-tagged jets, and by inverting
the identification criteria on signal jets when necessary.
\item Reconstruct the transverse mass spectrum in the signal and control regions. 
Transverse mass is defined in eq.~\ref{eq:zv_mtj} in Section~\ref{sec:zv_eventselection}. This is necessary because the presence of \MET\
in the final state prohibits the full reconstruction of the invariant mass of the resonant system.  Examine this spectrum for an excess.
\item In the case of no significant excess is observed, set 95\% confidence level (CL) upper limits on the production
cross section times branching fraction on various signal models.
\end{itemize}
 
