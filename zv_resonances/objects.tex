\section{Object definition}
\label{sec:zv_objects}

A concise summary of the objects and selections used in this analysis is presented in Table~\ref{tab:zv_objectselection} 
with more verbose descriptions provided in the sections that follow.
\\

\begin{table}[ht!]
\footnotesize
\begin{center}
\begin{tabular}{|c|c|c|c|}
\cline{1-4}
Object & Kinematics & Type,Quality & Additional \\ \hline
\multicolumn{1}{ |c|  }{\multirow{4}{*}{Electrons} } 
& $p_{T}>$7 GeV & LooseLH          &  \\
& $|\eta|<2.47$    & $d_{0}/\sigma(d_{0})<$5  &  \\
&                  & $|z_0 \sin\theta| < 0.5$ &  \\
&                  & isLooseTrackOnly isolation ($\epsilon\sim$99\%) &  \\
\cline{1-4}
\multicolumn{1}{ |c|  }{\multirow{4}{*}{Muons} } 
& $p_{T}>$7 or 25 GeV & Loose,Tight          &  \\
& $|\eta|<2.47$    & $d_{0}/\sigma(d_{0})<$3  &  \\
&                  & $|z_0 \sin\theta| < 0.5$ &  \\
&                  & isLooseTrackOnly isolation ($\epsilon\sim$99\%) &  \\
\cline{1-4}
\multicolumn{1}{ |c|  }{\multirow{4}{*}{Large-R Jets} } 
& $p_{T}>$200 GeV & anti-$k_{T}$ $R=$1.0                          & SmoothWZTagger \\
& $|\eta|<2.0$    & LCTopo                                        & 50\% WP \& 80\% WP  \\
&                 & Trimmed                                       &  For details, see \\
&                 & ($R_{\rm subjet} = 0.2$, $f_{\rm cut} = 5\%$) &   Section~\ref{sec:zv_largerjets} \\
\cline{1-4}
\multicolumn{1}{ |c|  }{\multirow{3}{*}{Small-R Jets} } 
& $p_{T}>$30 GeV if $|\eta|<2.5$       & anti-$k_{T}$ $R=$0.4 & $JVT>$0.59 \\
& $p_{T}>$35 GeV if $|\eta|=[2.5,4.5]$ & EMTopo               & if $p_{T}<$60 GeV,$|\eta|<2.4$ \\
&                                   &                      &  $MV2c10$ at 70\% efficiency \\
\cline{1-4}
%\multicolumn{1}{ |c|  }{\multirow{3}{*}{Track Jets} } 
%& $p_{T}>$10 GeV & anti-$k_{T}$ $R=$0.2 & $MV2c20$>-0.3098 \\
%& $|\eta|<2.5$   & Track                &  \\
%&                & $n_{tracks}\geq$2    &  \\
\cline{1-4}
\multicolumn{1}{ |c|  }{\multirow{3}{*}{\MET} } 
& \MET$>$250 GeV & MET\_TST &  Modified MET (\METnomu) \\
&               &          &  reconstructed by \\
&               &          &  manually removing muons \\
\cline{1-4}
\end{tabular}
\caption{A concise summary of the key object selections used in the analysis.  
For additional details, read the section.}
\label{tab:zv_objectselection}
\end{center}
\end{table}

\subsection{Leptons}
\label{sec:zv_leptons}
The main signature in this search is large \MET and an hadronic system consistent with a vector boson. 
No signature of prompt electrons or muons should present. 
However, the selection of charged leptons is important for two main reasons. 
Firstly, the number of leptons is used as the primary identifier to ensure that this analysis is orthogonal to the other 
various $VV$ search channels with charged lepton signature, such as $WZ\rightarrow \ell\nu qq$ and $ZZ\rightarrow \ell\ell qq$.
Secondly, events with charge leptons are used to define control regions to constrain the main backgrounds.
In particular, events with 0 lepton are used in the signal region, while events with 1 lepton are used for the Top and \W\ 
Control Regions, events with two leptons are used for the \Z\ Control Region. 
Only muons are used for the Control Regions since they allow to use the same \MET\ trigger used in the Signal Region.
\\

\subsubsection{Electrons}
Electron candidates are selected using the Egamma group recommendations. %~\cite{Egamma2015} and~\cite{Egamma2015workbook}.  
``Loose'' electrons are identified using the ``LooseLH'' identification 
criterion and are required to be central ($|\eta | < 2.47$), to have \pt $>$ 7 GeV,
and $d_0$ significance less than 5 and $|z_0 \sin\theta| < 0.5$. Isolation requirements are also applied using the {\sc LooseTrackOnly} working point.
\\

\subsubsection{Muons}
Muon candidates are selected using the Muon Combined Performance group recommendations. %~\cite{Muons2015} and~\cite{Muons2015workbook}. 
``Loose'' muons are identified using the ``LooseID'' identification criterion and 
are required to have \pt\ $>$ 7 GeV, $d_0$ significance less than 3 and $|z_0 \sin\theta| < 0.5$. Isolation is required 
using the {\sc LooseTrackOnly} working point. 
%For the one lepton control region, discussed in greater detail in Sections~\ref{sec:wtjets},
``Tight'' muons are defined using the ``Tight'' identification criterion and are required to have \pt\ $>$ 25 GeV.
\\

\subsection{Jets}
\label{sec:zv_jets}
Jets are used throughout this analysis for many purposes, for the identification of hadronically decaying \W and \Z bosons,
for the selection of events in the signal region that are free of contamination from QCD multijet events.
Both large and small radius jets are used in this analysis.
%Described in greater details in Section~\ref{sec:evpreselection}, the selection criteria for this relies on
%small radius jets constructed using calorimeter based information.
%Finally, small radius jets formed from inner detector tracks are used for the identification of b-jets in the one lepton control regions, discussed in greater detail in Sections~\ref{sec:wtjets}.
\\

\subsubsection{Large-R Jets}
\label{sec:zv_largerjets}
Large radius jets, also referred to as ``fat jets'', are used in this analysis to target the reconstruction of boosted, hadronically decaying vector bosons.
They are reconstructed with anti-$k_t$ algorithm with $R=1.0$. The detail of reconstruction and calibration is introduced in Section~\ref{sec:obj_jets}. 
The jets are required to have $p_{T}>$ 200 GeV and $|\eta| < 2$ in order to select high-\pt, central jets with a good overlap between the ID and the calorimeter.
\\

Large-R jets originating from the decays of boosted bosons have some peculiar features which make them different from QCD-originated jets.
These features are taken into account in a tool aimed at tagging the hadronically decaying bosons in order to distinguish them from QCD jets.
The recommendation for Run-2 analysis in 2017 is to use the smoothedWZ tagger. 
\\

\paragraph{SmoothedWZ tagger}

The smoothedWZ tagger consists of a cut on the jet mass and a cut on the \DTwoBetaOne jet substructure variable. 
\begin{itemize}
\item \textbf{Jet Mass} : This is intuitively related to the mass of the parent particle of the jet.  For
a boson jet, this corresponds to a mass close to the hard splitting in the $W/Z\rightarrow qq$ decay. 
In this paper, the combined mass~\cite{CombMass} is recommended instead of the calorimeter mass. 
The combined jet mass is a linear sum of the calorimeter and track-assisted masses, weighted such that, at each point 
in $(p_{T}, m/p_{T})$-space, the combined mass resolution is lower than the separate calorimeter mass and track-assisted mass resolutions.

\item \textbf{Energy Correlation Ratio} ($D_{2}$) : $D_{2}$ is ratio of two and three point energy correlation functions. 
It characterize very similar characteristics as $\tau_{21}$ N-subjettiness but are found to give better performance at the jet \pt\ increases. 
More details are discussed when we talked about jet reconstruction, see Section~\ref{sec:obj_jet_sub}.
\end{itemize}

The implementation smoothedWZ tagger consists of two pt-dependent selections, a combined mass window and a D2 cut. For 2015+2016
ZV resonances search and monoV analysis, the operating point designed to accept 50\% of the signal $W$ or $Z$ bosons is used.
Figure~\ref{fig:zv_wztagger_cuts} and Figure~\ref{fig:zv_wztagger_eff} from a study on WZ tagger \cite{WZtaggerIntNote} shows the cuts and 
efficiency for the $50\%$ efficiency working point.
\\

\begin{figure}[h]
\centering
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/W_SmoothTwoSidesMassCuts_50.pdf}}
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/Z_SmoothTwoSidesMassCuts_50.pdf}}\\
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/W_SmoothSubstructureCut_50.pdf}}
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/Z_SmoothSubstructureCut_50.pdf}}
\caption{\label{fig:zv_wztagger_cuts} The $W$ (left) and $Z$ (right) taggers use a $p_{\text{T}}$-dependent two-sided combined mass cut (top) 
and a one-sided upper cut on the $D_{2}$ substructure variable (bottom), which limits are shown here for the $50\%$ efficiency working point \cite{WZtaggerIntNote}.}
\end{figure}

\begin{figure}[h]
\centering
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/W_TaggingOverlay50.pdf}}
\subfloat[]{\includegraphics[width=.45\textwidth]{figures/monoV/wztagger/Z_TaggingOverlay50.pdf}}
\caption{\label{fig:zv_wztagger_eff} $W$ (top) and $Z$ (bottom) tagging efficiency and background rejection in dependence 
of the combined mass corrected jet $p_{\text{T}}$ \cite{WZtaggerIntNote}.}
\end{figure}


\subsubsection{Small-R Jets}
\label{sec:zv_smallrjets}
Small-R jets are reconstructed using the anti-$k_t$ algorithm with a distance parameter of $R = 0.4$. 
They are required to have \pt\ $>$ 20 GeV and $|\eta|<2.5$, while \pt\ $>$ 30 GeV is required for jets in $2.5 < |\eta| < 4.5$.  
Low-\pt\ (i.e. \pt\ $< 60$ GeV) jets coming from pileup are discarded by requiring 
JVT $> 0.59$~\cite{JVT}. A 70\% working point is applied for small-R jets b-tagging, used in the VBF category 
(Sec.~\ref{sec:zv_evselection_vbf})  and control regions~(Sec.~\ref{sec:zv_backgrounds}).
\\

\iffalse %%%%%%
\subsubsection{Track Jets}
\label{sec:zv_trackjets}

In addition to calorimeter based jets formed from topo-clusters, smaller radius trackjets are formed and used in the analysis. 
These jets are formed from inner detector tracks selected with the quality criteria used in~\cite{HiggsTagPreRec} and are constructed using the 
anti-$k_t$ algorithm with $R=0.2$. These jets are not calibrated using a JES correction as is done for the small-R or large-R jets.  They
are required to have $p_{T}$>10 GeV and $|\eta|$<2.5.  When necessary, these jets are identified as originating from
a b-quark using a multivariate approach based on the combination of track based observables in a boosted decision tree [ADDREF].  
That is, the MV2c20 discriminant is constructed from tracks and is required to be $MV2c20$>-0.0436 which is the 70\% signal 
efficiency working point for these jets.

At this point, it should be noted that this jet collection is used in the analysis to identify the presence of a b-jet in order to cleanly separate the
\W+jets and \ttbar control regions.  This choice was made because this analysis strongly overlaps in technique to the Higgs-based
counterparts (e.g. mono-H, VH resonance) which uses these track jets for Higgs identification.  Therefore, using these jets consistently 
throughout is foreseen to produce the most consistent results and most expediently treatable systematic uncertainties.
\fi %%%%

\subsection{Missing Transverse Energy (\MET)}
\label{sec:zv_met}

The missing transverse momentum is reconstructed according to the JetEtMiss group recommendations %\cite{METtwiki}
using the METMaker tool.
The \MET\ is calculated using reconstructed and fully calibrated objects including electrons, muons, photons, and small-R jets. 
Energy depositions not contained within a well-defined physics object listed above are taken into account through the measurement of
additional tracks and is contained in the track soft term (TST). 
This measurement of the MET, MET\_TST, is currently the baseline definition of the \MET for a wide majority of exotics analyses.  In the one
and two lepton control regions, studied only in the muon channel and described later, a modified version of the MET is used where the final MET vector is augmented
by adding the momentum vectors of muons back to the MET. 
%This has been discussed in a dedicated talk in the EtMiss subgroup meeting\footnote{\url{https://indico.cern.ch/event/459198/}}.
% and is discussed in greater length Appendix~\ref{sec:muonsinmet}.
%\textbf{NOTE : Need to check the kinematic selections on photons.} 
\\

\subsection{Overlap Removal}
\label{sec:zv_overlapremoval}

At the end of the object selection an overlap removal procedure is applied to avoid two or more selected analysis objects using a same particle. 
Two types of overlap removal are considered.
\begin{itemize}
    \item Overlap removal between electrons, muons, and jets.
        These objects are removed using the \texttt{OverlapRemovalTool} package, which implements the official overlap removal recommendations. 
For close-by electrons and small-$R$ jets, the jet is removed if the separation between the electron and jet is within
$\Delta R<$ 0.2; the electron is removed if the separation is within 0.2 $<\Delta R<$ 0.4.
For close-by muons and small-$R$ jets, the jet is removed if the separation between the muon and jet is within
$\Delta R<$ 0.2 and if, the jet has less than three tracks or the energy and momentum differences between the muon and the jet are small; the muon is removed if the separation is within $\Delta R<$ 0.4.
      
    \item Overlap removal between large-R jets and leptons. 
        Large-R jets are removed within a cone size of 1.0 around a selected lepton. 
        This is to avoid double counting, since a pair of close-by electrons or muons can be reconstructed as a large-R jet.
\end{itemize}
