\section{Data and Monte Carlo samples}
\label{sec:zv_samples}

\subsection{Data Samples}
\label{sec:zv_datasamples}
As mentioned, the full 2015 and 2016 dataset, corresponding to an integrated luminosity of \lumi. 
ATLAS provides lists of recorded p-p collision runs that are good  for physics analyses, namely "good run list". The good run lists used in the analysis are,

{
\tiny
\texttt{data15\_13TeV.periodAllYear\_DetStatus-v79-repro20-02\_DQDefects-00-02-02\_PHYS\_StandardGRL\_All\_Good\_25ns.xml}
\texttt{data16\_13TeV.periodAllYear\_DetStatus-v88-pro20-21\_DQDefects-00-02-04\_PHYS\_StandardGRL\_All\_Good\_25ns.xm}
%~\cite{twiki_GRL}.
}


\subsection{Trigger}
\label{sec:trigger}

MET triggers used in this analysis in data and Monte Carlo are summarized in Table~\ref{tab:zv_trigger}.

\begin{table}[h]
  \begin{center}
  \begin{tabular}{|l|c|c|}
  \hline

  Period                    & MET triggers                  \\ \hline
  2015                      & HLT\_xe70                     \\ \hline
  2016 A-D3                 & HLT\_xe90\_mht\_L1XE50        \\ \hline
  2016 D4-E4, F2-L11        & HLT\_xe110\_mht\_L1XE50       \\ \hline

  \hline
  \end{tabular}
  \end{center}
  \caption{The list of triggers used in the analysis.}
  \label{tab:zv_trigger}
\end{table}

The \MET\ at trigger level is reconstructed
% \footnote{https://indico.cern.ch/event/368856/session/25/contribution/197/attachments/1155380/1660546/HLT\_MET\_algos\_20150909.pdf}
using all cells in the full calorimeter system.  
Therefore muons are effectively not taken into account by default as they do not deposit an appreciable amount of energy in calorimeters.  
Thus, the \MET\ used in the trigger for events with a muon is the same as calculating \MET\ and then manually subtracting the contribution from muons, 
which is \METnomu in this analysis.
As a result, events collected with the muon trigger can be used to measure the trigger turn on curve of the \MET\ trigger.  
The \MET\ trigger efficiency for this study is  therefore measured by data and $W$+jets Monte Carlo by selecting a set of events using
 single and double muon triggers which are fully efficient for a much lower muon pt threshold.  
 The efficiency of the HLT\_xe110\_mht trigger are then studied as a function of \METnomu\, as shown in Figure~\ref{fig:zv_twolepton_cr_mettrigger_eff}.  
The results shows that an offline cut of \METnomu\ > 250 GeV applied to events selected with the HLT\_xe110\_mht trigger are fully efficient.  
Such a high \MET\ cut forms the basis of all selections that follow.  
%Detailed studies about the trigger efficiency can be found in Appendix~\ref{sec:mettrigeff}.
\\


\begin{figure}
\centering
\includegraphics[width=0.47\textwidth]{figures/zv_resonances/trigger/Teff_xe110_Wmunu.png} 
\includegraphics[width=0.47\textwidth]{figures/zv_resonances/trigger/SF_xe110_Wmunu.png} 
\caption{\label{fig:zv_twolepton_cr_mettrigger_eff} The trigger efficiency for events in data and $W$+jets Monte Carlo (left) for events seeded with
the single muon trigger with the ratio of the efficiency in data to that in Monte Carlo (right).}
\end{figure} 


\subsection{Monte Carlo Samples}
\label{sec:zv_mcsamples}
Monte Carlo simulated events are used for event selection optimization, evaluating signal acceptance,  background contamination and modeling,
systematic uncertainties and building templates for the statistical analysis. 
They are produced using the approved ATLAS event generation procedure, while the  ATLAS detector is simulated by GEANT4\cite{SOFT-2010-01}. %,\cite{Agostinelli:2002hh}.
\\

Background processes include the production of \W and \Z bosons in addition to partons 
(\Wjets and \Zjets), top quark production (\ttbar and single $t$ processes), and diboson production.
The \W and \Z bosons production in association with jets are simulated using \Sherpa 
2.2.1~\cite{Gleisberg:2008ta} generator in which matrix elements up to two partons are calculated
at NLO while LO calculations are used for 3 and 4 partons. The NNPDF30nnlo PDF set is used. 
The $W$/$Z$ + jets processes are normalized to the NNLO cross section.
For $t\bar{t}$, single top (s and t channels) and $Wt$ processes the  
\Powheg~\cite{Alioli:2010xd} generator is used and is interfaced to \Pythia~\cite{Sjostrand:2006za} 
for the parton shower, fragmentation and the underlying event.
Diboson production, including $WW$, $WZ$ and $ZZ$ is simulated with Sherpa 2.2.1 with the CT10 PDF set.
\\

%The main signal sample used is the narrow width approximation Higgs model(NWA), and also be interpreted into electroweak singlet model(EWS). 
As mentioned in Section~\ref{sec:zv_introduction}, signal samples used in this search are the narrow width approximation Higgs model (NWA) via 
ggF and VBF production; the HVT model~\cite{deBlas:2012qp} \cite{Pappadopulo:2014qza} 
which predicts the existence of a massive charged \wprime boson with couplings 
similar to those of the standard model \W boson, produced via both DY and VBF; 
the Randall-Sundrum model of extra dimensions~\cite{PhysRevLett.83.3370} \cite{PhysRevLett.83.4690}, 
which predicts the existence of a neutral spin-2 Kaluza-Klein graviton (\graviton) decaying to $ZZ$ pairs.
\\

All the signals considered in this analysis are summarized in Table~\ref{tab:zv_sig_sum}.
The NWA Higgs models are generated by the \Powheg\ generator interfaced to \Pythia\ with a mass range of 500 GeV to 3 TeV.
The HVT and the RS Graviton samples are generated with \Madgraph~\cite{Madgraph} interfaced to \Pythia\ with the signal mass range from 500 GeV to 5 TeV. 
\\

\begin{table}[h]
  \footnotesize
  \begin{center}
  \begin{tabular}{l|ccc}
  \hline

  Spin     & Signal      &  Cross section ($\sigma_{X} \times {\rm BR}(X\to VZ\to \nu\nu qq)$) & Intrinsic signal width            \\ \hline
  \multirow{2}{*}{0} &    ggF H    &  -  & 4~MeV  \\
                     &    VBF H    &  -  & 4~MeV  \\ \hline
  \multirow{2}{*}{1} &    DY HVT   &  1.13~pb@500 GeV, $2.90\times10^{-6}$~pb@5 TeV &  13.6 GeV @500 GeV, 128 GeV@5 TeV \\
                     &    VBF HVT  &  $3.18\times10^{-3}$~pb@500 GeV, $1.12\times10^{-8}$~pb@4 TeV  & 2.2 GeV@500 GeV, 13.8 GeV@4 TeV \\ \hline
  \multirow{2}{*}{2} &    \graviton , $\kappa/\overline{M}_{\rm Pl}=1$   & 0.63~pb@500 GeV, $4.57\times10^{-8}$~pb@5 TeV & 18.3 GeV@500 GeV, 322 GeV@5 TeV \\
                     &    \graviton , $\kappa/\overline{M}_{\rm Pl}=0.5$ &  -  & -  \\

  \hline
  \end{tabular}
  \end{center}
  \caption{Summary of signal models.} 
  \label{tab:zv_sig_sum}
\end{table}


\subsection{Analysis framework}
In this analysis, three different derivation samples are used: \texttt{HIGG5D1} with lepton veto skimming, \texttt{HIGG5D2} with
1 lepton skimming and \texttt{HIGG2D4} with two leptons skimming. The derivations with p-tag of \texttt{p2949} are used.
A analysis framework named \texttt{CxAOD}, which is commonly used in DBL group, is used in this analysis.
The final results presented in this chapter are based on \texttt{CxAOD} version \texttt{v28}, with \texttt{AnalysisBase-2.4.28}.
