\section{Statistical Interpretation}
\label{sec:zv_StatisticsProcedure}

The statistical treatment of this analysis uses a combined profile likelihood fit to binned discriminants in all categories and regions simultaneously. 
The definition of the likelihood and the configuration of the fit in terms of inputs and parameters are defined in this section.
The statistical framework of \texttt{WSMaker} is used in this analysis.
\\

\subsection{Likelihood definition}
\label{sec:zv_lhoodDef}

The statistical analysis of the data uses a binned likelihood function constructed as the product of Poisson probability terms,

\begin{equation}
    \mathrm{Pois}\,(n|\mu S+B)\left[ \prod_{b\in \text{bins}}^{n} \frac{\mu \nu^{\mathrm{sig}}_{b}+\nu^{\mathrm{bkg}}_{b}}{\mu S+B} \right],
\end{equation}

where $\mu$ is a signal strength parameter, which multiplies the expected signal yield $\nu^{\mathrm{sig}}_b$ in each histogram bin $b$, 
and $\nu^{\mathrm{bkg}}_b$ represents the background content for bin $b$. 
The dependence of the signal and background predictions on the systematic uncertainties is described by a set of nuisance parameters (NP) $\theta$, 
which are parameterized by Gaussian or log-normal priors. The latter are used for normalization uncertainties in order to maintain a positive likelihood.
The expected numbers of signal and background events in each bin are functions of $\theta$ and parameterized such that the rates in each category 
are log-normally distributed for a normally distributed $\theta$.
\\

The priors are used constrain the NPs to their nominal values within their assigned uncertainties.  
They are implemented via so-called penalty or auxiliary measurements added to the likelihood which will always increase when any nuisance parameter is shifted from the nominal value.
The likelihood function, $\mathcal{L} (\mu,\theta)$, is therefore a function of $\mu$ and $\theta$.
\\

The nominal fit result in terms of $\mu$ and $\sigma_{\mu}$ is obtained by maximizing the likelihood function with respect to all parameters.  
This is referred to as the maximized likelihood value, MLL.  
The test statistic $q_\mu$ is then constructed according to the profile likelihood: 
    $q_\mu = 2 \ln (\mathcal{L} (\mu, \hat{\hat{\theta_\mu}})/\mathcal{L} (\hat{\mu}, \hat{\theta}))$, 
where $\hat{\mu}$ and $\hat{\theta}$ are the parameters which maximize the likelihood (with the constraint $0 \leq \hat{\mu} \leq \mu$), 
and $\hat{\hat{\theta}}_\mu$ is the nuisance parameter value that maximize the likelihood for a given $\mu$. 
This test is used to measure the compatibility of the background-only model with the observed data and for exclusion intervals derived with the $CL_s$ method~\cite{Cowan:2010js}.
The limit set on $\mu$ is then translated into a limit on the signal cross section times branching ratio, $\sigma \times \mbox{BR}(S\rightarrow ZV\rightarrow \nu\nu qq)$,
using the theoretical cross section and branching ratio for the given signal model. 
\\

\subsection{Fit inputs and variables}
\label{sec:zv_fitconf}

A combined profile likelihood fit is performed across all regions simultaneously.
%The analysis discriminating distributions are arranged as the outer product of category, region, regime, and subregime. Here category refers to the selection for VBF or ggF signal, region refers to signal (SR) and control regions (CR), regime refers to the merged and resolved jet analysis objects used in selection, and subregime refers to 01-tag and 2-tag split for regular jets and W/Z-tagged vs. low-purity selection for large-R jets.
For each categories and regions, the input to the likelihood is the final \mt\ distribution, built with the \MET\ (\METnomu\ in the leptonic control regions) and the fat jet.
\\

The overarching principle of the analysis design is to provide a \Z control region from the two muons sample and top and \W\ control regions from the one muon sample. 
% In all of these, the two jet mass side bands are used in order to normalize the backgrounds in the fit.
%This strategy serves the analysis well with the addition of a top CR made from the tagged subregime but requiring an $e\mu$-pair instead of a matching lepton pair.
The detailed description of selection criteria used to define SR and CRs can be found in sections \ref{sec:zv_evselection_cat}, \ref{sec:zv_Zjets}, and \ref{sec:zv_wtjets}.
Minor backgrounds are taken from MC simulation, normalized to the cross-sections
defined in \ref{sec:zv_mcsamples} whereas the primary backgrounds $Z+$jets, $W+$jets and top are constrained entirely by the fit.
\\

\iffalse %%%%%%%%%%%%%
The complete list of regions used in the fit is shown in Table~\ref{tab:zv_fitregions}.  

\begin{table}[htb!]
    \begin{center}
      \begin{tabular}{|c|c|c|}
        \hline
        \multirow{3}{*} {Cat.}      &     \multirow{3}{*} {Region}      & \multicolumn{4}{|c|}{$llqq$ channel}\\
            \cline{3-6}
            & & \multicolumn{2}{|c|}{Resolved}                 & \multicolumn{2}{|c|}{Merged}   \\
            \cline{3-6}
            & & untagged & tagged & W/Z tagged & low purity  \\
            \hline
            \multirow{3}{*} {ggF} & SR & \mt & \mt   & \mt & \mt    \\
            \cline{2-6}
             & ZCR & \mt & \mt   & \mt & \mt    \\
            \cline{2-6}
             & TopCR & --- & \mt  & --- & ---    \\
            \hline
            \multirow{2}{*} {VBF} & SR & \mt & \mt   & \mt & \mt    \\
            \cline{2-6}
             & ZCR & \mt & \mt   & \mt & \mt    \\
            \hline
        \end{tabular}
        \caption{\label{tab:fitregions} Summary of the regions entering the likelihood fit and the
            distribution used in each. Rows with ``---'' indicate that the region is not included in the fit.
        ``SR'' stands for the signal regions and ``CR'' for the control regions.}
    \end{center}
\end{table}
\fi %%%%%%%%%%%

\subsection{Nuisance parameters: normalization and systematic uncertainties}
\label{sec:zv_nps}

All systematic uncertainties enter the profile likelihood fit as nuisance parameters.  
Two different types of nuisance parameters are used: floating parameters and parameters with priors.
\\

For the most significant backgrounds, those which the analysis is designed to constrain, no prior probability 
distribution is assigned to the normalization and the contribution are therefore floating. 
The fit contains four (six in spin-0 and spin-1 studies) freely-floating normalization parameters 
that are constrained by the signal and control regions described above.

\begin{description}
    \item [Signal:] Signal strength.
    \item [Background:] The following scale factors are used for the backgrounds in the different regions:
        \begin{description}
            \item[\Zjets:]  Overall $Z$~boson production normalization for high and low purity regions. 
                In the cases of spin-0 and spin-1, an additional normalization factor is set for the VBF category.
            \item[\Wjets:]  Overall $W$~boson production normalization for high and low purity regions. 
                In the cases of spin-0 and spin-1, an additional normalization factor is set for the VBF category.
            \item[Top:]  Overall top quark production normalization for high and low purity regions. 
        \end{description}
\end{description}

Due to the low statistics in the Top VBF category, no additional VBF normalization factor is set for top quark.
\\

A nuisance parameter with a prior corresponds to a systematic uncertainty where there is a prior constraint on 
the value of the parameter from designated studies (for systematics we use log-norm priors). 
The fit contains 50 nuisance parameters from experimentally-derived uncertainties (see \ref{sec:zv_ExpSyst}) and 
11 nuisance parameters from modeling uncertainties (see \ref{sec:zv_BkgModeling}), in addition to the floating normalization parameters.
Nuisance parameters from signal acceptance uncertainties, due to PDF and ISR/FSR, are also included in the fit (2 NPs).
During the fitting procedure, nuisance parameters are pruned if they are found to be negligible.
\\


The statistical uncertainties for the background MC samples are taken into account in the profile likelihood 
using a light weight version of the Barlow-Beeston method as implemented in HistFactory~\cite{HistFactory}.  
This adds an extra nuisance parameter representing the statistical uncertainty on the total MC background in each bin, which is completely uncorrelated across bins.  
These nuisance parameters are not added to all bins but only those bins where the relative statistical uncertainty in the bin is above the threshold of 5\%.
\\

\subsection{Binning}
By studies on signal resolutions at different mass points, a same binning for the transverse mass of the \vvqq\ system
from $500$ GeV to $6500$ GeV is applied in all the signal regions in the fitting for different spins, \par
{\small
    \centering
$500,550,610,690,790,910,1050,1200,1380,1570,1780,2010,$ \\
$2260,2530,2820,3130,3450,3800,4160,4540,5000,5750,6500$. \par
}
Due to the statistical fluctuation in control regions, we decided to use one bin from $500$ GeV to $6500$ GeV for control regions.
