\section{Results}
\label{sec:zv_result}

This section describes the results of the analysis, including fits to Asimov data as well as fits to the collected data.
Also shown are the pulls of the nuisance parameters, the overall normalization factors from the simultaneous fit in the signal and control regions, 
and the correlation matrix between all of the nuisance parameters in the analysis.
\\

\subsection{Results for the spin-2 hypothesis}
\label{sec:RSG_results}
The graviton model is studied in two different couplings, $\kappa/\overline{M}_{\rm Pl}=1$ and $\kappa/\overline{M}_{\rm Pl}=0.5$. 
As mentioned in Section~\ref{sec:zv_eventselection}, in the spin-2 hypothesis we only consider high- and low-purity regions, without splitting in VBF/ggF.
\\

As a closure test, we generated and fit Asimov data from our likelihood profile.
We expect the fit to show best fit background normalizations of unity, as well as well-constrained nuisance parameter values.
Figure~\ref{fig:zv_rsg_asi_pulls} show the pulls of the nuisance parameters and the best fit values of the background normalizations and 
Figure~\ref{fig:zv_rsg_asi_corr} shows the correlation matrix between all of the nuisance parameters considered in the analysis.
The ranking of the nuisance parameters based on their individual power to influence the test fit can be found in Fig.~\ref{fig:zv_rsg_asi_ranking}.
\\

There are some notable highly constrained nuisance parameters, such as \texttt{FatJet\_JMR/D2R} and several modeling systematics 
like \texttt{Z\_MadGraph}, \texttt{ttbar\_Herwig}. The modeling systematics are estimated conservatively so the constraint is expected. 
The constraints for \texttt{JMR/D2R} are due to the large priors, which cause large normalization uncertainty, so they are constrained from the fit. 
%Table~\ref{tab:zv_np_ranking} shows the contribution to the uncertainty on the best-fit signal cross section from the dominant systematic uncertainties 
%for the combined \llqq\ and \vvqq\ search. 
At the low resonance mass region, the systematic uncertainties and data statistical uncertainties are
 about equally important, while at the high resonance mass region, the data statistical uncertainties play a more important role. 
\\

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_RSG_asimov0.pdf}
\caption{\label{fig:zv_rsg_asi_pulls} Pulls on the nuisance parameters and best fit background normalization factors from the test fit (mu=0) 
to Asimov data for the spin-2 hypothesis with m\textsubscript{G} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/corr_RSG_asimov0.pdf}
\caption{\label{fig:zv_rsg_asi_corr} Correlations on the nuisance parameters and the background normalization factors due to 
the test fit for the spin-2 hypothesis with m\textsubscript{G} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/zv_resonances/results/17102_RSG_final_1600_asimovDataAt0_pulls_1600.pdf}
\caption{\label{fig:zv_rsg_asi_ranking}Ranking of the nuisance parameters and the background normalization factors scale factors based on
 their power to influence the test fit for the spin-2 hypothesis with m\textsubscript{G} $=1600 \gev$ .}
\end{center}
\end{figure}



Figure~\ref{fig:zv_rsg_postfit_SR} shows the invariant mass distributions in signal regions after the background only fit has been applied. 
The yields of the Standard Model background and data after the fit in all regions are listed in Table~\ref{tab:zv_rsg_yields}.
\\

In addition, the pulls of the nuisance parameters from this fit are shown in Figure~\ref{fig:zv_rsg_data_pulls} and the nuisance parameter 
correlations are shown in Figure~\ref{fig:zv_rsg_data_corr}. 
The ranking of the nuisance parameters based on their individual power to influence the fit can be found in Figure~\ref{fig:zv_rsg_data_ranking}.
The best-fit normalization factors for the fit can be found in Table~\ref{tab:zv_rsg_bestfitvals}.
\\

\begin{figure}[htb!]
\begin{center}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/RSGZZvvqq1600_0ptag1pfat0pjet_0ptv_HighPSROfRSG_vvJMT.pdf}}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/RSGZZvvqq1600_0ptag1pfat0pjet_0ptv_LowPSROfRSG_vvJMT.pdf}}
\caption{\label{fig:zv_rsg_postfit_SR} Observed and expected distributions of \mt\ of the \vvqq\ system in 
    (a) high-purity and (b) low-purity signal regions in the spin-2 study. The expected signal distributions 
shown are from a 1.6 TeV RS graviton with $\sigma \times$ BR$(G \to ZZ) =$ 6 fb. The blue dash line in the ratio pad
shows the prefit/postfit ratio of backgrounds.}
\end{center}
\end{figure}


\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_RSG_data0.pdf}} \\
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_Kp5RSG_data0.pdf}}
    \caption{\label{fig:zv_rsg_data_pulls}Pulls on the nuisance parameters and global scale factors due to the background-only fit 
    for a 1.6 TeV RS graviton signal with couplings of (a) $\kappa/\overline{M}_{\rm Pl}=1$, (b) $\kappa/\overline{M}_{\rm Pl}=0.5$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_RSG_data0.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_Kp5RSG_data0.pdf}}
    \caption{\label{fig:zv_rsg_data_corr}Correlations on the nuisance parameters and global scale factors due to the background-only fit
     for a 1.6 TeV RS graviton signal with couplings of (a) $\kappa/\overline{M}_{\rm Pl}=1$, (b) $\kappa/\overline{M}_{\rm Pl}=0.5$.}
\end{center}
\end{figure}


\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_RSG_final_1600_obsData_pulls_1600.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_Kp5RSG_final_1600_obsData_pulls_1600.pdf}}
    \caption{\label{fig:zv_rsg_data_ranking}Ranking of the nuisance parameters and global scale factors due to the background-only fit
     for a 1.6 TeV RS graviton signal with couplings of (a) $\kappa/\overline{M}_{\rm Pl}=1$, (b) $\kappa/\overline{M}_{\rm Pl}=0.5$.}
\end{center}
\end{figure}



\begin{table}[htb!]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        $Z $ & $0.977 \pm 0.064$ \\
        \hline
        $W$ & $1.027 \pm 0.069$ \\
        \hline
        Top & $1.025 \pm 0.112$ \\
        \hline
    \end{tabular}
    \caption{Spin-2 best-fit values of the global normalization factors from the background-only fit to all regions.}
    \label{tab:zv_rsg_bestfitvals}
\end{table}


\begin{table}
    \centering
    \small
    \begin{tabular}{lccccccc}
        \hline
        \hline
        RS $G^*$  &  Z+jets  &  W+jets  &  ttbar  &  SingleTop  &  Diboson  &  Bkg  &  Data  \\
        \hline
          \\
          \multicolumn{8}{c}{SR}  \\
          \hline
          HP  &  2811 $\pm$ 114  &  1845 $\pm$ 143  &  1517 $\pm$ 146  &  228 $\pm$ 28  &  574 $\pm$ 81  &  6977 $\pm$ 76  &  6990  \\
          \hline
          LP  &  11346 $\pm$ 361  &  7059 $\pm$ 437  &  2567 $\pm$ 264  &  355 $\pm$ 28  &  911 $\pm$ 105  &  22240 $\pm$ 145  &  22265  \\
          \hline
            \\
            \multicolumn{8}{c}{ZCR}  \\
            \hline
            HP  &  1128 $\pm$ 27  &  0  &  14.4 $\pm$ 2.1  &  1.21 $\pm$ 0.46  &  40.0 $\pm$ 5.4  &  1184 $\pm$ 26  &  1226  \\
            \hline
            LP  &  1886 $\pm$ 37  &  0  &  29.7 $\pm$ 4.6  &  2.81 $\pm$ 0.42  &  58.1 $\pm$ 7.4  &  1976 $\pm$ 38  &  1917  \\
            \hline
              \\
              \multicolumn{8}{c}{WCR}  \\
              \hline
              HP  &  179 $\pm$ 10  &  6424 $\pm$ 319  &  2357 $\pm$ 281  &  474 $\pm$ 43  &  228 $\pm$ 30  &  9664 $\pm$ 95  &  9621  \\
              \hline
              LP  &  255 $\pm$ 15  &  9151 $\pm$ 414  &  3478 $\pm$ 384  &  613 $\pm$ 50  &  292 $\pm$ 35  &  13791 $\pm$ 114  &  13820  \\
              \hline
                \\
                \multicolumn{8}{c}{TopCR}  \\
                \hline
                HP  &  2.19 $\pm$ 0.37  &  83.5 $\pm$ 20.6  &  1264 $\pm$ 48  &  206 $\pm$ 21  &  10.3 $\pm$ 3.5  &  1566 $\pm$ 36  &  1557  \\
                \hline
                LP  &  10.1 $\pm$ 2.0  &  274 $\pm$ 64  &  1712 $\pm$ 88  &  263 $\pm$ 24  &  22.8 $\pm$ 5.6  &  2284 $\pm$ 46  &  2291  \\
                \hline
                \hline
            \end{tabular}
    \caption{Spin-2 best-fit values of the global yields for the Standard Model backgrounds from the background-only ($\mu=0$) fit, 
    as well as the total number of data candidates in all regions.}
    \label{tab:zv_rsg_yields}
\end{table}


The expected and observed limits on the graviton production cross section times the $ZZ \rightarrow \nu\nu qq$ branching ratio, $\sigma \times BR$, are shown in Fig.~\ref{fig:zv_RSG_lim}.
\\

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/RSGZZvvqq_limit_False_log.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/Kp5RSGZZvvqq_limit_False_log.pdf}}
    \caption{\label{fig:zv_RSG_lim}Expected and observed limits on $\sigma \times BR$ for the graviton production with couplings of (a) $\kappa/\overline{M}_{\rm Pl}=1$, (b) $\kappa/\overline{M}_{\rm Pl}=0.5$. The red dotted curve represents the theory cross section for the graviton model.}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results for the spin-1 hypothesis}
\label{sec:zv_HVT_results}
In the spin-1 hypothesis, the HVT W' from the VBF production and the Drell-Yan production are studied. 
Therefore VBF and Drell-Yan categories are defined as mentioned in Section~\ref{sec:zv_eventselection},  both split into high- and low-purity regions.

As a closure test, we generated and fit Asimov data from our likelihood profile.
We expect the fit to show best fit background normalizations of unity, as well as well-constrained nuisance parameter values.
Figure~\ref{fig:zv_HVT_asi_pulls} show the pulls of the nuisance parameters and the best fit values of the background normalizations and 
Figure~\ref{fig:zv_HVT_asi_corr} shows the correlation matrix between all of the nuisance parameters considered in the analysis.
The ranking of the nuisance parameters based on their individual power to influence the test fit can be found in Figure~\ref{fig:zv_HVT_asi_ranking}.


\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_HVT_asimov0.pdf}
\caption{\label{fig:zv_HVT_asi_pulls} Pulls on the nuisance parameters and best fit background normalization factors from the test fit (mu=0) to 
Asimov data for the spin-1 hypothesis with m\textsubscript{$w'$} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/corr_HVT_asimov0.pdf}
\caption{\label{fig:zv_HVT_asi_corr} Correlations on the nuisance parameters and the background normalization factors due to the test fit
 for the spin-1 hypothesis with m\textsubscript{$W'$} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/zv_resonances/results/17102_HVT_final_1600_asimovDataAt0_pulls_1600.pdf}
\caption{\label{fig:zv_HVT_asi_ranking}Ranking of the nuisance parameters and the background normalization factors scale factors based on
 their power to influence the test fit for the spin-1 hypothesis with m\textsubscript{$W'$} $=1600 \gev$ .}
\end{center}
\end{figure}

%\clearpage

Figure~\ref{fig:zv_hvt_postfit_SR} shows the invariant mass distributions in signal regions after the background only fit has been applied. 
The yields of the Standard Model background and data after the fit in all regions are listed in Table~\ref{tab:zv_HVT_yields}.

In addition, Figure~\ref{fig:zv_HVT_data_pulls} shows the pulls of the nuisance parameters from this fit and 
Figure~\ref{fig:zv_HVT_data_corr} shows the nuisance parameter correlations. 
The ranking of the nuisance parameters based on their individual power to influence the fit are shown in Figure~\ref{fig:zv_HVT_data_ranking}.
The best-fit normalization factors for the fit can be found in Table~\ref{tab:zv_hvt_bestfitvals}.

\begin{figure}[htb!]
\begin{center}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/vbfHVTWZvvqq1600_0ptag1pfat0pjet_0ptv_HighPSROfVBFHVT_vvJMT.pdf}}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/vbfHVTWZvvqq1600_0ptag1pfat0pjet_0ptv_LowPSROfVBFHVT_vvJMT.pdf}} \\
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/HVTWZvvqq1600_0ptag1pfat0pjet_0ptv_HighPSROfHVT_vvJMT.pdf}}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/HVTWZvvqq1600_0ptag1pfat0pjet_0ptv_LowPSROfHVT_vvJMT.pdf}}
\caption{\label{fig:zv_hvt_postfit_SR} Observed and expected distributions of \mt\ of the \vvqq\ system in 
    (a) VBF high-purity, (b) VBF low-purity signal, (c) Drell-Yan high-purity and (d) Drell-Yan low-purity signal regions in the spin-1 study. 
    The expected signal distributions shown are from a 1.6 TeV HVT W' from the VBF production in (a) (b) and the DY production in (c) (d)
    , with $\sigma \times$ BR$(W' \to WZ) =$ 12 fb. The blue dash line in the ratio pad shows the prefit/postfit ratio of backgrounds.}
\end{center}
\end{figure}

\begin{table}
    \centering
    \small
    \begin{tabular}{lccccccc}
        \hline
        \hline
        HVT  &  Z+jets  &  W+jets  &  ttbar  &  SingleTop  &  Diboson  &  Bkg  &  Data  \\
        \hline
          \\
          \multicolumn{8}{c}{SR}  \\
          \hline
          DY HP  &  2380 $\pm$ 93  &  1612 $\pm$ 113  &  1280 $\pm$ 118  &  214 $\pm$ 20  &  517 $\pm$ 62  &  6005 $\pm$ 73  &  6038  \\
          \hline
          DY LP  &  8734 $\pm$ 266  &  5687 $\pm$ 318  &  1828 $\pm$ 204  &  267 $\pm$ 22  &  726 $\pm$ 87  &  17243 $\pm$ 127  &  17250  \\
          \hline
          VBF HP  &  25.1 $\pm$ 5.9  &  17.9 $\pm$ 4.1  &  33.7 $\pm$ 5.3  &  3.33 $\pm$ 1.02  &  7.23 $\pm$ 1.42  &  87.3 $\pm$ 6.5  &  78.0  \\
          \hline
          VBF LP  &  135 $\pm$ 23  &  47.7 $\pm$ 17.6  &  58.0 $\pm$ 9.9  &  5.28 $\pm$ 1.51  &  12.2 $\pm$ 2.6  &  258 $\pm$ 16  &  260  \\
          \hline
            \\
            \multicolumn{8}{c}{ZCR}  \\
            \hline
            DY HP  &  1176 $\pm$ 28  &  0  &  13.6 $\pm$ 2.3  &  1.14 $\pm$ 0.4  &  42.5 $\pm$ 5.3  &  1233 $\pm$ 28  &  1269  \\
            \hline
            DY LP  &  1647 $\pm$ 34  &  0  &  28.1 $\pm$ 3.7  &  2.97 $\pm$ 0.41  &  50.2 $\pm$ 6.3  &  1728 $\pm$ 35  &  1677  \\
            \hline
            VBF HP  &  25.2 $\pm$ 5.1  &  0  &  1.86 $\pm$ 1.87  &  0  &  1.4 $\pm$ 1.44  &  28.5 $\pm$ 5.3  &  29.0  \\
            \hline
            VBF LP  &  28.3 $\pm$ 5.8  &  0  &  1.58 $\pm$ 1.46  &  0  &  2.28 $\pm$ 1.39  &  32.1 $\pm$ 5.3  &  32.0  \\
            \hline
              \\
              \multicolumn{8}{c}{WCR}  \\
              \hline
              DY HP  &  173 $\pm$ 9  &  6491 $\pm$ 311  &  2192 $\pm$ 278  &  444 $\pm$ 40  &  226 $\pm$ 28  &  9528 $\pm$ 94  &  9474  \\
              \hline
              DY LP  &  249 $\pm$ 13  &  9151 $\pm$ 406  &  3275 $\pm$ 381  &  592 $\pm$ 47  &  281 $\pm$ 33  &  13551 $\pm$ 113  &  13594  \\
              \hline
              VBF HP  &  1.86 $\pm$ 0.46  &  71.5 $\pm$ 13.8  &  60.6 $\pm$ 10.0  &  10.4 $\pm$ 2.2  &  6.56 $\pm$ 1.59  &  150 $\pm$ 9  &  149  \\
              \hline
              VBF LP  &  2.99 $\pm$ 0.92  &  107 $\pm$ 21  &  86.0 $\pm$ 15.1  &  15.6 $\pm$ 2.8  &  8.33 $\pm$ 1.71  &  220 $\pm$ 12  &  226  \\
              \hline
                \\
                \multicolumn{8}{c}{TopCR}  \\
                \hline
                DY HP  &  2.38 $\pm$ 0.43  &  87.4 $\pm$ 19.4  &  1229 $\pm$ 47  &  194 $\pm$ 21  &  10.4 $\pm$ 3.2  &  1523 $\pm$ 36  &  1526  \\
                \hline
                DY LP  &  10.5 $\pm$ 2.0  &  302 $\pm$ 60  &  1649 $\pm$ 86  &  248 $\pm$ 22  &  20.6 $\pm$ 4.8  &  2231 $\pm$ 45  &  2224  \\
                \hline
                VBF HP  &  0.01 $\pm$ 0.01  &  1.23 $\pm$ 0.54  &  30.7 $\pm$ 4.2  &  4.94 $\pm$ 1.44  &  0.23 $\pm$ 0.19  &  37.1 $\pm$ 4.5  &  31.0  \\
                \hline
                VBF LP  &  0.18 $\pm$ 0.16  &  5.34 $\pm$ 1.73  &  45.7 $\pm$ 5.5  &  8.29 $\pm$ 1.94  &  1.37 $\pm$ 0.89  &  60.9 $\pm$ 6.1  &  66.0  \\
                \hline
                \hline
            \end{tabular}
            \caption{Spin-1 best-fit values of the global yields for the Standard Model backgrounds from the background-only ($\mu=0$) fit, 
            as well as the total number of data candidates in all regions.}
            \label{tab:zv_HVT_yields}
        \end{table}

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_VBFHVT_data0.pdf}} \\
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_HVT_data0.pdf}}
    \caption{\label{fig:zv_HVT_data_pulls}Pulls on the nuisance parameters and global scale factors due to the background-only fit 
    for a 1.6 TeV HVT $W'$ signal from (a) the VBF production and (b) the Drell-Yan production.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_VBFHVT_data0.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_HVT_data0.pdf}}
    \caption{\label{fig:zv_HVT_data_corr}Correlations on the nuisance parameters and global scale factors due to the background-only fit
     for a 1.6 TeV HVT $W'$ signal  from (a) the VBF production and (b) the Drell-Yan production.}
\end{center}
\end{figure}


\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_VBFHVT_final_1600_obsData_pulls_1600.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_HVT_final_1600_obsData_pulls_1600.pdf}}
    \caption{\label{fig:zv_HVT_data_ranking} Ranking of the nuisance parameters and global scale factors due to the background-only fit
     for a 1.6 TeV HVT $W'$ signal from (a) the VBF production and (b) the Drell-Yan production.}
\end{center}
\end{figure}



\begin{table}[htb!]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        & Normalization Factor & VBF Ratio \\
        \hline
        $Z $ & $0.999 \pm 0.062$  & $0.887 \pm 0.250$ \\
        \hline
        $W$ & $1.109 \pm 0.092$  & $0.657 \pm 0.169$ \\
        \hline
        Top & $1.034 \pm 0.117$ & N/A \\
        \hline
    \end{tabular}
    \caption{Spin-1 best-fit values of the global normalization factors from the fit to all the regions in the analysis. 
    The normalization factors are applied to both DY and VBF categories, 
    while the VBF ratio are additional normalization factors applied to VBF categories only.}
    \label{tab:zv_hvt_bestfitvals}
\end{table}



The expected and observed limits on the HVT production cross section times the $WZ \rightarrow \nu\nu qq$ branching ratio, 
$\sigma \times BR$, are shown in Figure~\ref{fig:zv_HVT_lim}.
\\

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/VBFHVTWZvvqq_limit_False_log.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/HVTWZvvqq_limit_False_log.pdf}}
    \caption{\label{fig:zv_HVT_lim}Expected and observed limits on $\sigma \times BR$ for the HVT W' from (a) the VBF production and (b) the Drell-Yan production. 
    The red dotted curve represents the theory cross section for the HVT model.}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Results for narrow-width scalar model}
\label{sec:zv_res_higgs}

Similar to the spin-1 studies, to study the narrow width Higgs from the VBF production and the ggF production, we defined VBF and ggF categories. 
These categories are further split into high- and low-purity regions. Detailed descriptions can be find in Section~\ref{sec:zv_eventselection}.

As a closure test, we generated and fit Asimov data from our likelihood profile.
We expect the fit to show best fit background normalizations of unity, as well as well-constrained nuisance parameter values.
Figure~\ref{fig:zv_ggH_asi_pulls} shows the pulls of the nuisance parameters and the best fit values of the background normalizations and 
Figure~\ref{fig:zv_ggH_asi_corr} shows the correlation matrix between all of the nuisance parameters considered in the analysis.
The ranking of the nuisance parameters based on their individual power to influence the test fit can be found in Figure~\ref{fig:zv_ggH_asi_ranking}.


\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_ggH_asimov0.pdf}
\caption{\label{fig:zv_ggH_asi_pulls} Pulls on the nuisance parameters and best fit background normalization factors from the test fit (mu=0)
 to Asimov data for the spin-0 hypothesis with m\textsubscript{$H$} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/corr_ggH_asimov0.pdf}
\caption{\label{fig:zv_ggH_asi_corr} Correlations on the nuisance parameters and the background normalization factors due to the test fit
 for the spin-0 hypothesis with m\textsubscript{$H$} $=1600 \gev$.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/zv_resonances/results/17102_ggH_final_1600_asimovDataAt0_pulls_1600.pdf}
\caption{\label{fig:zv_ggH_asi_ranking}Ranking of the nuisance parameters and the background normalization factors scale factors based on
 their power to influence the test fit for the spin-0 hypothesis with m\textsubscript{$H$} $=1600 \gev$ .}
\end{center}
\end{figure}

%Figs.~\ref{fig:hvt_postfit_SR}-\ref{fig:hvt_postfit_TopCR} show the invariant mass distributions in all regions after the background only fit has been applied.
Figure~\ref{fig:zv_ggH_postfit_SR} shows the invariant mass distributions in signal regions after the background only fit has been applied. 
The yields of the Standard Model background and data after the fit in all regions are listed in Table~\ref{tab:zv_ggH_SRyields}.

In addition, Figire~\ref{fig:zv_ggH_data_pulls} shows the pulls of the nuisance parameters from this fit and 
Figure~\ref{fig:zv_ggH_data_corr} shows the nuisance parameter correlations. 
The ranking of the nuisance parameters based on their individual power to influence the fit are shown in Figure~\ref{fig:zv_ggH_data_ranking}.
The best-fit normalization factors for the fit can be found in Table~\ref{tab:zv_ggH_bestfitvals}.

\begin{figure}[htb!]
\begin{center}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/VBFH1600NWZZvvqq_0ptag1pfat0pjet_0ptv_HighPSROfVBFH_vvJMT.pdf}}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/VBFH1600NWZZvvqq_0ptag1pfat0pjet_0ptv_LowPSROfVBFH_vvJMT.pdf}} \\
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/ggH1600NWZZvvqq_0ptag1pfat0pjet_0ptv_HighPSROfggH_vvJMT.pdf}}
\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/zv_resonances/results/ggH1600NWZZvvqq_0ptag1pfat0pjet_0ptv_LowPSROfggH_vvJMT.pdf}}
\caption{\label{fig:zv_ggH_postfit_SR} Observed and expected distributions of \mt\ of the \vvqq\ system in 
    (a) VBF high-purity, (b) VBF low-purity signal, (c) ggF high-purity and (d) ggF low-purity signal regions in the spin-0 study. 
    The expected signal distributions shown are from a 1.6 TeV Higgs from the VBF production in (a) (b) and the ggF production in (c) (d)
    , with $\sigma \times$ BR$(H \to ZZ) =$ 6 fb. The blue dash line in the ratio pad shows the prefit/postfit ratio of backgrounds.}
\end{center}
\end{figure}


\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_VBFH_data0.pdf}} \\
    \subfloat[]{\includegraphics[width=.8\textwidth]{figures/zv_resonances/results/NP_ggH_data0.pdf}}
    \caption{\label{fig:zv_ggH_data_pulls}Pulls on the nuisance parameters and global scale factors due to the background-only fit for a 1.6 TeV Higgs signal 
    from (a) the VBF production and (b) the ggF production.}
\end{center}
\end{figure}

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_VBFH_data0.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/corr_ggH_data0.pdf}}
    \caption{\label{fig:zv_ggH_data_corr}Correlations on the nuisance parameters and global scale factors due to the background-only fit for a 1.6 TeV Higgs signal 
    from (a) the VBF production and (b) the ggF production.}
\end{center}
\end{figure}


\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_VBFH_final_1600_obsData_pulls_1600.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/17102_ggH_final_1600_obsData_pulls_1600.pdf}}
    \caption{\label{fig:zv_ggH_data_ranking} Ranking of the nuisance parameters and global scale factors due to the background-only fit for a 1.6 TeV Higgs signal 
    from (a) the VBF production and (b) the ggF production.}
\end{center}
\end{figure}



\begin{table}[htb!]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        & Normalization Factor & VBF Ratio \\
        \hline
        $Z $ & $0.995 \pm 0.061$  & $0.878 \pm 0.176$ \\
        \hline
        $W$ & $1.051 \pm 0.095$  & $0.660 \pm 0.175$ \\
        \hline
        Top & $1.033 \pm 0.114$ & N/A \\
        \hline
    \end{tabular}
    \caption{Spin-0 best-fit values of the global normalization factors from the fit to all the regions in the analysis. The normalization factors are applied to both ggF and VBF categories,
    while the VBF ratio are additional normalization factors applied to VBF categories only.}
    \label{tab:zv_ggH_bestfitvals}
\end{table}


\begin{table}
    \centering
    \small
    \begin{tabular}{lccccccc}
        \hline
        \hline
        Higgs  &  Z+jets  &  W+jets  &  ttbar  &  SingleTop  &  Diboson  &  Bkg  &  Data  \\
        \hline
          \\
          \multicolumn{8}{c}{SR}  \\
          \hline
          VBF HP  &  35.3 $\pm$ 5.7  &  19.7 $\pm$ 4.4  &  40.2 $\pm$ 6.9  &  3.01 $\pm$ 1.2  &  8.02 $\pm$ 1.6  &  106 $\pm$ 6  &  105  \\
          \hline
          VBF LP  &  166 $\pm$ 19  &  71.6 $\pm$ 19.4  &  73.4 $\pm$ 12.9  &  7.25 $\pm$ 1.93  &  15.2 $\pm$ 3.2  &  333 $\pm$ 16  &  335  \\
          \hline
          ggF HP  &  2813 $\pm$ 105  &  1838 $\pm$ 132  &  1459 $\pm$ 145  &  214 $\pm$ 25  &  543 $\pm$ 75  &  6870 $\pm$ 76  &  6888  \\
          \hline
          ggF LP  &  11189 $\pm$ 314  &  6996 $\pm$ 383  &  2497 $\pm$ 268  &  350 $\pm$ 27  &  883 $\pm$ 102  &  21917 $\pm$ 144  &  21936  \\
          \hline
            \\
            \multicolumn{8}{c}{ZCR}  \\
            \hline
            VBF HP  &  20.3 $\pm$ 2.7  &  0  &  1.26 $\pm$ 0.31  &  0  &  1.04 $\pm$ 0.25  &  22.6 $\pm$ 2.7  &  27.0  \\
            \hline
            VBF LP  &  35.6 $\pm$ 4.5  &  0  &  1.82 $\pm$ 0.73  &  0  &  2.05 $\pm$ 0.36  &  39.5 $\pm$ 4.6  &  34.0  \\
            \hline
            ggF HP  &  1112 $\pm$ 27  &  0  &  11.9 $\pm$ 7.9  &  1.43 $\pm$ 1.61  &  39.1 $\pm$ 5.1  &  1165 $\pm$ 26  &  1200  \\
            \hline
            ggF LP  &  1853 $\pm$ 39  &  0  &  25.4 $\pm$ 11.8  &  2.62 $\pm$ 3.61  &  55.1 $\pm$ 7.1  &  1936 $\pm$ 37  &  1884  \\
            \hline
              \\
              \multicolumn{8}{c}{WCR}  \\
              \hline
              VBF HP  &  1.76 $\pm$ 0.33  &  70.2 $\pm$ 14.0  &  63.0 $\pm$ 10.3  &  10.9 $\pm$ 2.2  &  6.44 $\pm$ 1.55  &  152 $\pm$ 9  &  149  \\
              \hline
              VBF LP  &  3.24 $\pm$ 0.76  &  107 $\pm$ 21  &  87.8 $\pm$ 15.4  &  14.8 $\pm$ 2.7  &  8.71 $\pm$ 1.79  &  222 $\pm$ 12  &  226  \\
              \hline
              ggF HP  &  178 $\pm$ 9  &  6403 $\pm$ 307  &  2252 $\pm$ 273  &  461 $\pm$ 42  &  224 $\pm$ 28  &  9520 $\pm$ 94  &  9474  \\
              \hline
              ggF LP  &  251 $\pm$ 12  &  9089 $\pm$ 401  &  3333 $\pm$ 379  &  602 $\pm$ 49  &  282 $\pm$ 33  &  13559 $\pm$ 113  &  13594  \\
              \hline
                \\
                \multicolumn{8}{c}{TopCR}  \\
                \hline
                VBF HP  &  0.02 $\pm$ 0.01  &  1.27 $\pm$ 0.55  &  30.1 $\pm$ 4.1  &  5.2 $\pm$ 1.39  &  0.24 $\pm$ 0.2  &  36.8 $\pm$ 4.5  &  31.0  \\
                \hline
                VBF LP  &  0.19 $\pm$ 0.16  &  5.51 $\pm$ 2.01  &  45.3 $\pm$ 5.7  &  8.46 $\pm$ 2.04  &  1.31 $\pm$ 0.95  &  60.8 $\pm$ 6.1  &  66.0  \\
                \hline
                ggF HP  &  2.38 $\pm$ 0.42  &  87.2 $\pm$ 20.4  &  1223 $\pm$ 47  &  198 $\pm$ 20  &  9.85 $\pm$ 3.18  &  1521 $\pm$ 35  &  1526  \\
                \hline
                ggF LP  &  10.6 $\pm$ 2.2  &  291 $\pm$ 65  &  1655 $\pm$ 85  &  254 $\pm$ 23  &  21.7 $\pm$ 5.2  &  2233 $\pm$ 45  &  2224  \\
                \hline
                \hline
            \end{tabular}
        \caption{Spin-0 best-fit values of the global yields for the Standard Model backgrounds from the background-only ($\mu=0$) fit, as well as the total number of data candidates in all regions.}
        \label{tab:zv_ggH_SRyields}
\end{table}


The expected and observed limits on the narrow-width scalar production cross section times the $ZZ \rightarrow \nu\nu qq$ branching ratio, $\sigma \times BR$, 
in gluon-gluon fusion (ggF) and vector boson fusion (VBF) channels are shown in Figure~\ref{fig:zv_Higgs_lim}.
\\

\begin{figure}[htb!]
\begin{center}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/VBFHZZvvqq_limit_False_log.pdf}}
    \subfloat[]{\includegraphics[width=.47\textwidth]{figures/zv_resonances/results/ggHZZvvqq_limit_False_log.pdf}}
    \caption{\label{fig:zv_Higgs_lim} Expected and observed limits on $\sigma \times BR$ for the narrow width Higgs from (a) the VBF production and (b) the ggF production.}
\end{center}
\end{figure}
