\section{Event Selection}
\label{sec:zv_eventselection}

This section describes the event selection used to define the signal region. 
First, a trigger requirement and a set of preselection cuts is applied to remove events originating from mis-measured multijet events.
After the preselection, a number of requirements on the event topology are made involving \MET\ (identifying $Z\rightarrow\nu\nu$) and jets,
the selections for jets (identifying $Z \rightarrow qq$).
Besides, a pile-up reweighting is applied to MC events, in order to match the pile-up conditions in the data.
\\

\subsection{Event Preselection}
\label{sec:zv_evpreselection}

Events are required to satisfy the following quality criteria.
\begin{itemize}
\item \MET trigger :  MET triggers for different run periods are listed in Table~\ref{tab:zv_trigger};
% Details on the trigger are given in appendix~\ref{sec:mettrigeff}
\item In data only, apply the good run list to events;
\item Require at least one reconstructed vertex with at least 2 associated tracks;
\item In data, veto events that contain bad TileCalo or LAr data due to noise bursts or data corruption as well as events with Core and SCT error flags;
\item Veto events if any small-R jet that passes the JVT requirement with \pt\ > 20 GeV fails
the {\sc LooseBadJet} jet cleaning requirement, to ensure a good measurement on \MET.
\end{itemize}


After the above preselection, events are required to have  \MET\ $>$ 250 GeV in order to be in the boosted regime.
After this preselection a large number of events coming from multijet processes is still contained in the data sample.
These type of events are removed by the so-called anti-QCD selections on the following three event level quantities.
\begin{itemize}
\item \MPT\ $>$ 50 GeV : a track-based missing transverse momentum, \mptvec, defined as the negative vectorial sum
 of the transverse momentum of all good-quality inner detector tracks, is used to further suppress the multijet background. 
 Even if a jet in a dijet event is mismeasured in the calorimeter, the tracks will still be well-reconstructed.  
Therefore, they will balance to a greater extent and the overall track-based MET will be at lower values with the generation of \MET\
is caused by fluctuations in the charged to neutral fractions of the jets in such events.
\item min($\Delta\Phi$(\MET, small-R jets)) > 0.4 : in the case of a dijet event in which one is mismeasured, 
the \MET\ will point in the same direction of the lower \pt\ jet.  
Therefore, dijet events are expected to reside at low values of this observable. 
In the calculation of this observable,  when no small-R jets are found in the event based on the criteria in Section~\ref{sec:zv_smallrjets}, 
the event is considered to pass this cut.
\item $\Delta\Phi$ (\MET, \MPT ) < 1 : In the case of an event with real \MET, \metvec should be aligned with \mptvec as they both are representative of 
the direction of undetected energy flow, whether it be measured in the tracker or the calorimeter.  
However, in the case of a dijet event with a mismeasured jet, \metvec and \mptvec will each align with one of the two jets (they may not align to the same jet).  
Therefore, the observable will either reside at extremely high or low values, most clearly seen in Figure~\ref{fig:zv_antiqcdcuts} (right side).  
After selecting on the other two observables, it is observed that the events are biased to reside primarily at high values.
\end{itemize}


The variables and values chosen for these selections are based on the physical intuition described before.
The exact selection values are chosen by examining the spectrum of these observables. 
Regions where data is poorly modeled by the backgrounds that are expected to be dominant in the topology of interest (\Zjets, \Wjets, \ttbar) are removed.
The comparison of data and Monte Carlo backgrounds is shown in Figure~\ref{fig:zv_antiqcdcuts}.  To demonstrate the effect 
that this has on the description of the data by the MC backgrounds, the \MET\ spectra is shown in Figure~\ref{fig:zv_metprogression} 
both before any selections are applied, and then after the anti-QCD cuts are applied.  Furthermore, distributions shown in 
Figure~\ref{fig:zv_fatjetprogression} is the \pt\ and mass of the leading large-R jet in the event, after all the three anti-QCD cuts.
Detailed checks showed that the amount of residual multijet events after the anti-QCD cuts is negligible. 
Since the large-R jet requirements are not applied yet, discrepancy could still be observed in the Data/MC comparison. 
%More plots show the Data/MC agreement are shown in Appendix~\ref{sec:zv_antiQCD_final}.
\\

%\newpage
%\clearpage

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMPT_SpcMET_DMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMinDPhi_SpcMET_DMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDPhiCaloTrkMET_SpcMET_DMETOfIncl_Prefitlog.pdf}} \\ [-2ex]
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMPT_SpcTrkMET_DTrkMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMinDPhi_SpcTrkMET_DTrkMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDPhiCaloTrkMET_SpcTrkMET_DTrkMETOfIncl_Prefitlog.pdf}} \\ [-2ex]
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMPT_SpcMinDPhi_DMinDPhiOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMinDPhi_SpcMinDPhi_DMinDPhiOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDPhiCaloTrkMET_SpcMinDPhi_DMinDPhiOfIncl_Prefitlog.pdf}} \\ [-2ex]
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMPT_SpcDPhiCaloTrkMET_DDPhiCaloTrkMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMinDPhi_SpcDPhiCaloTrkMET_DDPhiCaloTrkMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.30\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDPhiCaloTrkMET_SpcDPhiCaloTrkMET_DDPhiCaloTrkMETOfIncl_Prefitlog.pdf}} \\[-1ex]
\centering
\caption{\label{fig:zv_antiqcdcuts} Observed and expected distributions of the three variables used to reject QCD background. 
    These are track-based \MET\ (left), min($\Delta\Phi$(\MET, small-R jets))(middle), and $\Delta\Phi$ (\MET, track-\MET ) (right) following the selections from top to bottom. 
    The expected signal distributions shown are from an 1.6 TeV Graviton with $\sigma \times$ BR$(G \to ZZ) = 1$ pb.}
\end{figure}

%\newpage
%\clearpage

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcMET_DMETOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcTrkMET_DTrkMETOfIncl_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcMinDPhi_DMinDPhiOfIncl_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/antiQCD/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcDPhiCaloTrkMET_DDPhiCaloTrkMETOfIncl_Prefitlog.pdf}}
\centering
 \caption{\label{fig:zv_metprogression}
 Observed and expected distributions of \MET\ spectrum for (a) before the anti-QCD selections,
 (b) after track-based \MET\ cut, (c) after min($\Delta\Phi$(\MET, small-R jets)) selection, 
 (d) all anti-QCD cuts applied.
    The expected signal distributions shown are from an 1.6 TeV Graviton with $\sigma \times$ BR$(G \to ZZ) = 1$ pb.}
\end{figure}

%\newpage
%\clearpage

% do we really need it? maybe just after the preselection!!!
\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetPt_SpcKin_DKinOfRSG_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetMass_SpcKin_DKinOfRSG_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_fatjetprogression} Observed and expected distributions of (a) mass of the large-R jet and (b) of the large-R jet
    after anti-QCD selections have been applied. The expected signal distributions shown are from an 1.6 TeV Graviton 
    with $\sigma \times$ BR$(G \to ZZ) = 1$ pb.}
\end{figure}

%\newpage
%\clearpage


\subsection{VBF Selection}
\label{sec:zv_evselection_vbf}
%As mentioned in 
A vector boson fusion (VBF) category is introduced to interpolate the contribution from VBF signals.
Besides the signal jet, in the VBF channel, two VBF jets radiated from the quarks of the incoming protons are required.
The two jets are usually in the forward sections of the detector and have a large separation in pseudorapidity.
Show in Figure~\ref{fig:zv_vbf_vars_pre} are $M^{tag}_{jj}$ and $|\Delta\eta^{tag}_{jj}|$ distributions of the VBF jets.
Figure~\ref{fig:zv_vvqq_VBF} shows the $M^{tag}_{jj}$ and $|\Delta\eta^{tag}_{jj}|$ distributions of the VBF tag jets after all
selections applied. Discrepancy between the data and the MC predictions in these distributions are observed, however, 
the profile-likelihood-ratio fit (Section~\ref{sec:zv_StatisticsProcedure}) corrects such mismatching, 
leading to reasonable agreement between the data and simulation.
% A study on VBF category optimization is summarized in Appendix~\ref{sec:zv_vbf}.
\\

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMVBFjj_SpcVBFPresel_DVBFPreselOfVBFH_GlobalFit_conditionnal_mu0log.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDeltaEtaVBFjj_SpcVBFPresel_DVBFPreselOfVBFH_GlobalFit_conditionnal_mu0log.pdf}}
\centering
 \caption{\label{fig:zv_vbf_vars_pre}
 The invariant mass (left) and $|\Delta\eta^{tag}_{jj}|$ (right) spectrum for VBF jets. $M^{tag}_{jj}$ and $|\Delta\eta^{tag}_{jj}|$ cuts 
 are not applied and the signal region selections on large-$R$ jet are not applied.}
%%%%%%%%%%%\textbf{done with CxAOD 21-04, need to update}
\end{figure}

%%%%% vvqq VBF observables
\begin{figure}[!htb]
\centering
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/zv_resonances/vvqq/Region_Y2016_L0_isMVA0_T0_incTag1_Fat1_incFat1_J2_incJet1_BMin0_distMVBFjj_DSRs_GlobalFit_conditionnal_mu0_logy.pdf}}
  \subfloat[]{\includegraphics[width=0.495\textwidth]{figures/zv_resonances/vvqq/Region_Y2016_L0_isMVA0_T0_incTag1_Fat1_incFat1_J2_incJet1_BMin0_distDeltaEtaVBFjj_DSRs_GlobalFit_conditionnal_mu0_logy.pdf}}
  \caption{\label{fig:zv_vvqq_VBF} Observed and expected distributions of dijet (a) invariant mass and 
  (b) pseudorapidity separation of the two tag jets of the VBF $H\to ZZ\to \vvqq$ search, combining all signal regions. 
  Background contributions in these distributions are estimated from MC simulations.  For illustration, expected distributions from the ggF production 
  of an 1.6~TeV Higgs boson with $\sigma\times{\rm BR}(H\to ZZ)=6$~fb are also shown. 
  The bottom panes show the ratio of the observed data to the predicted background. 
  The uncertainty on the total background prediction, shown as bands, combines statistical and systematic contributions. 
  The blue triangles in the bottom panes indicate bins where the ratio is outside the vertical range of the plot.}
\end{figure}


By the optimal results from the study, the VBF jets are selected from non-btagged small-R jets by the following requirements: 

\begin{itemize}
\item opposite signs of $\eta$,
\item $M^{tag}_{jj} > 630 GeV$,
\item $|\Delta\eta^{tag}_{jj}| > 4.7$.
\end{itemize}

If more than one pair of jets pass the selection, the one with the highest $M^{tag}_{jj}$ is selected. 
In VBF events, the two selected jets are required to be outside of $|\Delta R| > 1.5$ of the leading large-R jet
to ensure there is no double counting of the jet energy. 
Events failing the VBF selection or having any VBF jets overlap with the fatjet are categorized as ggF or DY.
For \graviton search, where no VBF production is considered, no such VBF selection is applied, thus an inclusive category
is used. 
\\

\subsection{Event Categorization}
\label{sec:zv_evselection_cat}

For both VBF category and ggF/DY category, and the inclusive category for \graviton search, events are divided into different regions in order to 
maximize the total sensitivity of the analysis.
The overall analysis strategy is targeted at reconstructing events in the boosted topology.
\\

\subsubsection{High purity signal region}

In the high-purity signal region the hadronically-decaying boson is reconstructed using a large-R jet, therefore events are required to have at least one of those.
In events that contain multiple large-R jets, the candidate boson jet is always taken to
be the highest \pt jet in the event. In this region the large-R jet is required to be tagged as a vector boson.
This is achieved using the ``boson tagging'' technique described in section~\ref{sec:zv_largerjets} which relies on a two-sided cut on the large-R jet mass 
and a one-sided cut on the substructure variable \DTwoBetaOne at 50\% working point. Summarizing, the high purity region is defined as follows:
\begin{itemize}
\item No loose electrons or muons as defined in Section~\ref{sec:zv_leptons}.
\item At least 1 large-R jet passes kinematics requirements in Section~\ref{sec:zv_largerjets}.
\item Pass the \DTwoBetaOne\ selection at 50\% working point described in Section~\ref{sec:zv_largerjets}.
\item Pass the jet mass selection at 50\% working point described in Section~\ref{sec:zv_largerjets}.
\end{itemize}


Figure~\ref{fig:zv_fatjetD2} shows the \DTwoBetaOne\ variable before the splitting in high- 
and low-purity categories.
\\

\begin{figure}
\centering
\subfloat[]{\includegraphics[width=0.6\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetD2_SpcKin_DKinOfRSG_Prefitlog.pdf}}
\caption{\label{fig:zv_fatjetD2} \DTwoBetaOne\ of the leading large-R jet in the event before splitting in categories.
    The expected signal distribution shown is from an 1.6 TeV Graviton with $\sigma \times$ BR$(G \to ZZ) = 1$ pb.}
\end{figure}

\subsubsection{Low purity signal region}

As explained in section~\ref{sec:zv_largerjets}, a cut on the \DTwoBetaOne\ substructure variable ensures 
that the selected large-R jet has a two-pronged structure, as expected for a boosted vector boson decaying
hadronically. This is true in a "medium" kinematic range. Outside of this kinematic range, the 
single-sided cut on the substructure variable turns out to kill the signal efficiency: 
in the highly-boosted regime this happens because the two quarks coming from the boson decay are 
so collimated that they are reconstructed in a single sub-jet within the large-R jet;
while in the low-boost regime it happens because additional subjets may arise from radiation thus 
spoiling the two-pronged structure of the large-R jet.
In order to recover the efficiency loss just described, a specific ``low purity'' region is 
considered: in this region the boson tagging requirements are loosened to 80\% tagging efficiency.
\\

A Summary of signal region event selection is list in Table~\ref{tab:sr_selection}.
\\

\begin{table}[ht!]
\small
\begin{center}
\begin{tabular}{|l|c|c|}
\cline{1-3}
\multicolumn{3}{ |c|  }{Event Selection} \\ \hline
                            &   High Purity             &  Low Purity                                   \\ \hline
Trigger                     &  \multicolumn{2}{c|}{\METtriggerF\ and \METtriggerS}                      \\ \hline
Lepton Veto                 &  \multicolumn{2}{c|}{ veto any ``loose'' lepton with \pt $>$ 7 GeV}       \\ \hline
\MET                        &  \multicolumn{2}{c|}{\MET\ > 250 GeV }                                   \\ \hline
\MPT                        &  \multicolumn{2}{c|}{\MPT\ > 50 GeV}                                      \\ \hline
min($\Delta\Phi$(\MET,small-R jets))   &  \multicolumn{2}{c|}{ $>$ 0.4}                                 \\ \hline
$\Delta\Phi$(\MET,\MPT)     &  \multicolumn{2}{c|}{ $< $ 1}                                             \\ \hline
\multirow{3}{*}{Jets}       &  \multicolumn{2}{c|}{\phantom{a}}                         \\
                            &  \multicolumn{2}{c|}{$\geqslant$ 1 large-R jets}          \\
                            &  \multicolumn{2}{c|}{\phantom{a}}                         \\
\cline{1-3}
Jet substructure                      &   Boson tagging  & Boson tagging  \\
                                                     &  (W/Z-substructure at 50\% WP)  & !(50\% WP) \&\& (80\% WP) \\ \hline
Jet Mass                      &   Boson tagging  & Boson tagging  \\
                                                     &  (W/Z-mass at 50\% WP)  & !(50\% WP) \&\& (80\% WP) \\ \hline

\end{tabular}
\caption{A summary of signal region event selection. For spin 0 and spin 1 searches, each of these regions is defined for
 VBF and ggF (DY) categories mentioned in Section~\ref{sec:zv_evselection_vbf}.}
\label{tab:sr_selection}
\end{center}
\end{table}


The final discriminant used in both the high purity and the low purity categories of this analysis is the transverse mass of the \vvqq\ system, 
defined as, 
\begin{equation}
\label{eq:zv_mtj}
\mt^{2} = (E_{\text{T}}^J + \MET)^2 - (\vec{p}_{\text{T}}^{J} + \metvec)^2
\end{equation}
where $E_{\text{T}}^J$ and $\vec{p}_{\text{T}}^{J}$ are the transverse energy and the vectorial transverse momentum of the large-R jet.
It is necessary to use the transverse mass instead of the full invariant mass due to the presence of the \MET\ in the final state. 
The spectrum of this variable is shown in Figure~\ref{fig:zv_mt_sr}.
\\


\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSR_DHighPSROfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSR_DLowPSROfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSR_DHighPSROfggH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSR_DLowPSROfggH_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_mt_sr} Observed and expected distributions of the transverse mass of the \vvqq\ system in 
    (a) the VBF high purity, (b) the VBF low purity (c) the ggF high purity, (d) the ggF low purity signal regions with all selections applied. 
    The expected signal distributions shown are from the VBF production in (a) (b) and the ggF production in (c) (d), 
    both of an 1.6 TeV Higgs boson with $\sigma \times$ BR$(H \to ZZ) = 1$ pb.}
\end{figure}

The acceptances as a function of the resonance mass are shown for the three signal models in Figure~\ref{fig:acc}.
\\

\begin{figure}
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/signal/acc_ggH_weighted_linear.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/signal/acc_VBFH_weighted_linear.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/signal/acc_HVT_weighted_linear.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/signal/acc_VBFHVT_weighted_linear.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/signal/acc_RSG_weighted_linear.pdf}}
\caption{\label{fig:acc} Acceptance $\times$ efficiency as a function of the resonance mass for the signal models considered in this search: 
(a) Spin-0 ggH, (b) Spin-0 VBFH, (c) Spin-1 HVT (d) Spin-1 VBFHVT, (e) Spin-2 RSG.
The value is estimated by $N$(\text{events\ passed\ selection}) / (Cross\ section * Luminosity).}
\end{figure}


\subsection{Zero Lepton Validation Region}
\label{sec:zv_evselection_vr}
Although the set of events selected with zero leptons is nominally the signal region, it is also
useful to use these events to define a validation region that can be used to test the data-MC 
agreement.
%the constraint of the $W$+jets and $Z$+jets backgrounds that enter the search after the fit has been performed.
This is achieved by selecting events with the baseline set of selections outlined in 
Section~\ref{sec:zv_evpreselection} but fails the jet mass requirements that define the signal region, 
while the other cuts mentioned in the Section~\ref{sec:zv_evselection_cat} are kept. 
In the high-purity validation region, this means accepting events where the jet mass is outside
the mass window of 50\% and boson tagging working point while in the low-purity validation region,
a jet mass rejection at 80\% working point is applied with the exact selections please refer to 
Section~\ref{sec:zv_largerjets}. To make it more clear, Figure~\ref{fig:zv_hp_lp} shows the 
categorization for zero lepton regions with the region priority described in 
Section~\ref{sec:zv_evselection_prior} applied.
\\

In the Spin 0 study, the high- and low-purity validation regions are further split into 
VBF and ggF categories described in Section~\ref{sec:zv_evselection_vbf}, following the same strategy
in the signal region. For this set of events, the validation plots for VBF categories are shown in 
Figure~\ref{fig:zv_vbf_hp_vr_var} and Figure~\ref{fig:zv_vbf_lp_vr_var}. The plots for ggF categories 
are shown in Figure~\ref{fig:zv_ggf_hp_vr_var} and Figure~\ref{fig:zv_ggf_lp_vr_var}. The transverse mass
spectra in these four different categories in the validation regions are shown in 
Figure~\ref{fig:zv_0lep_vr_mt}.  
%The use of this as a validation region is shown later, in Figure~\ref{fig:validation_region}.
\\

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDeltaEtaVBFjj_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMVBFjj_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetMass_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetPt_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetD2_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_vbf_hp_vr_var} Observed and expected distributions of (a) $|\Delta\eta^{tag}_{jj}|$, (b) $m^{tag}_{jj}$, (c) \MET, 
(d) mass of the large-R jet, (e) \pt of the large-R jet, (f) D2 of the large-R jet in the VBF high-purity validation region. 
The Higgs boson is assumed to have a $\sigma \times$ BR$(H \to ZZ)$ value of 1 pb, from the VBF production.}
\end{figure}

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distDeltaEtaVBFjj_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMVBFjj_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetMass_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetPt_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.4\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetD2_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_vbf_lp_vr_var} Observed and expected distributions of (a) $|\Delta\eta^{tag}_{jj}|$, (b) $m^{tag}_{jj}$, (c) \MET, 
(d) mass of the large-R jet, (e) \pt of the large-R jet, (f) D2 of the large-R jet in the VBF low-purity validation region. 
The Higgs boson is assumed to have a $\sigma \times$ BR$(H \to ZZ)$ value of 1 pb, from the VBF production.}
\end{figure}

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcSB_DHighPSBOfggH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetMass_SpcSB_DHighPSBOfggH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetPt_SpcSB_DHighPSBOfggH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetD2_SpcSB_DHighPSBOfggH_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_ggf_hp_vr_var} Observed and expected distributions of (a) \MET, (b) mass of the large-R jet, (c) \pt of the large-R jet, (d) D2 of the large-R jet 
in the ggF high-purity validation region. The Higgs boson is assumed to have a $\sigma \times$ BR$(H \to ZZ)$ value of 1 pb, from the ggF production.}
\end{figure}

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distMET_SpcSB_DLowPSBOfggH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetMass_SpcSB_DLowPSBOfggH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetPt_SpcSB_DLowPSBOfggH_Prefitlog.pdf}}
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distfatJetD2_SpcSB_DLowPSBOfggH_Prefitlog.pdf}}
\centering
\caption{\label{fig:zv_ggf_lp_vr_var} Observed and expected distributions of (a) \MET, (b) mass of the large-R jet, (c) \pt of the large-R jet, (d) D2 of the large-R jet 
in the ggF high-purity validation region. The Higgs boson is assumed to have a $\sigma \times$ BR$(H \to ZZ)$ value of 1 pb, from the ggF production.}
\end{figure}

\begin{figure}[htbp]
\centering
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSB_DHighPSBOfVBFH_Prefitlog.pdf}} 
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF1_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSB_DLowPSBOfVBFH_Prefitlog.pdf}} \\
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP1_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSB_DHighPSBOfggH_Prefitlog.pdf}} 
\subfloat[]{\includegraphics[width=0.47\textwidth]{figures/zv_resonances/zerolep/Region_isVBF0_isHP0_Y2016_L0_isMVA0_T0_incTag1_J0_incJet1_Fat1_incFat1_BMin0_distvvJMT_SpcSB_DLowPSBOfggH_Prefitlog.pdf}} 
\centering
\caption{\label{fig:zv_0lep_vr_mt} Observed and expected distributions of the transverse mass of the \vvqq\ system in 
    (a) the VBF high purity, (b) the VBF low purity (c) the ggF high purity, (d) the ggF low purity validation regions with all selections applied. 
    The expected signal distributions shown are from the VBF production in (a) (b) and the ggF production in (c) (d), 
    both of an 1.6 TeV Higgs boson with $\sigma \times$ BR$(H \to ZZ) = 1$ pb.}
\end{figure}
 
\subsection{Regions Priorities}
\label{sec:zv_evselection_prior}

%In this analysis not all the categories are orthogonal by construction: as an example the boosted and the low purity are, since in the former the large-R jet is required to satisfy the
%conditions on \DTwoBetaOne\ for the boson tagging, while the latter is designed to recover the events which fail this requirement. On the other hand,
%in particular in the low mass region, events can be reconstructed with both the resolved category and the boosted or low purity categories. In order to avoid double counting,
%priorities have been assigned to each category.
The final sensitivity of the analysis is given by the combination of the channels that are used.
In this respect, different solutions have been compared and the final choice is made in order to obtain the best expected limit combining all the categories.
%Figure~\ref{fig:limcomp} shows the comparison of the expected limits obtained using the different selection priorities (the ``resolved'' is included as a reference).
After these studies, events are categorized as follows:
\begin{itemize}
\item In spin-0 and spin-1 studies, check if the event passes VBF selections:
    \begin{itemize}
    \item if it does, pass it to the VBF category;
    \item if not, pass it to the ggF category for spin-0 and the DY category for spin-1;
    \end{itemize}
\item For events in each category of spin-0, spin-1 and the inclusive one of spin-2:
    \begin{itemize}
    \item check if the event falls in the high-purity SR.
    \item if not, check if the event falls in the low-purity SR.
    \item if not, the same chain is applied for validation and control regions
    \end{itemize}
\end{itemize}


The event categorization of zero lepton regions is shown in Figure~\ref{fig:zv_hp_lp}.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\textwidth]{figures/zv_resonances/zerolep/HP_LP_plot.pdf}
\centering
\caption{\label{fig:zv_hp_lp} The event categorization of zero lepton regions. In the spin 0 study, 
each of the region in this plot is further split into VBF and ggF categories.}
\end{figure}
 

%\begin{figure}[htbp]
%\centering
%\includegraphics[width=0.7\textwidth]{figures/santa.jpg}
%\caption{\label{fig:limcomp} Expected limits obtained for the different priorities assigned to each category. The limit is computed including the experimental systematic uncertainties.}
%\end{figure}
